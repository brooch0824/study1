using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SmallerUpdate : MonoBehaviour
{
    [Range(0.1f, 0.99f)] public float speedMulti;
    //作用：每个小方块出生后就开始缩小
    void Update()
    {
        transform.localScale *= speedMulti;
        if (transform.localScale.magnitude <= 0.05f)
        {
            Destroy(transform.parent.gameObject);
        }
    }
}
