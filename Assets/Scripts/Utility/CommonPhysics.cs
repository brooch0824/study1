using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommonPhysics : MonoBehaviour
{
    public LayerMask m_LayerMask;
    bool canHit = true;
    Collider[] hitColliders;

    void Awake()
    {
    }

    void FixedUpdate()
    {
        //生成一个看不见的碰撞盒
        hitColliders = Physics.OverlapBox(gameObject.transform.position, transform.localScale / 2, Quaternion.identity, m_LayerMask);
        MyCollisions();
    }

    void MyCollisions()
    {
        if (hitColliders.Length > 0 && canHit == true)
        {
            LoadManager.GameOncall();
            canHit = false;
        }
        else if (hitColliders.Length == 0)
        {
            canHit = true;
        }
    }

}
