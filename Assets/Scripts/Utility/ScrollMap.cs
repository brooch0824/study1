using UnityEngine;

public class ScrollMap : MonoBehaviour
{
    [SerializeField] Vector2 offset;
    Material mat;

    // Update is called once per frame

    void Awake()
    {
        mat = GetComponent<MeshRenderer>().material;
    }

    void Update()
    {
        mat.mainTextureOffset += offset * Time.deltaTime;
    }
}
