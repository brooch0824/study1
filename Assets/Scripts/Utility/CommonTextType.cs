//"选择初始波次 <size=2>>>>>>>>>>>>>>>>>>>>>您达到的最远波次×80%</size>\n\n" + "17" + "\n\n您的初始分数 <size=2>>>>>>>>>>>>>>>>>>>>> 由您在该波次的最大分控制 </size>\n\n" + "590"

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

public class CommonTextType : MonoBehaviour
{
    public List<string> comment_state1 = new();
    public List<string> comment_state2 = new();
    public List<string> comment_state3 = new();

    int curWave;
    int maxWave;
    Coroutine CWaitForType;
    Coroutine CType;

    void OnEnable()
    {
        IUserInput.m_OnMouseDrag += OnMouseDrag;
    }
    void OnDisable()
    {
        IUserInput.m_OnMouseDrag -= OnMouseDrag;
    }

    void Start()
    {
        // input =
        // "选择初始波次\n<size=3>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>只能选择10的倍数哦</size>\n\n\n" +
        // "<size=8>17</size>" +
        // "\n\n\n您的初始分数\n<size=3>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>您在该波次的最大分控制 </size>\n\n\n" +
        // "<size=8>590</size>";
        //input = "选择初始波次 >>>您达到的最远波次×80%\n\n" + "17" + "\n\n您的初始分数 >>> 由您在该波次的最大分控制\n\n" + "590";

        maxWave = (UserData.UTIIEW.Keys.Max() - 1) / 10 * 10 + 1;
        curWave = maxWave;

        CWaitForType = StartCoroutine(WaitForType(null));
        CType = StartCoroutine(Type(GenerateText(curWave)));
    }



    string GenerateText(int _initWave)
    {
        //根据当前波次的刷新局和玩家游玩总局数的差值，生成不同的文本
        string _comment = null;
        Debug.Log(UserData.UserGamesCount - UserData.UTIIEW[_initWave].games_count);
        switch (UserData.UserGamesCount - UserData.UTIIEW[_initWave].games_count)
        {
            case -1:
            case 0:
            case 1:
            case 3:

                var a = UnityEngine.Random.Range(0, comment_state1.Count);
                _comment = comment_state1[a];

                break;
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
                var b = UnityEngine.Random.Range(0, comment_state2.Count);
                _comment = comment_state2[b];
                break;
            default:
                var c = UnityEngine.Random.Range(0, comment_state3.Count);
                _comment = comment_state3[c];
                break;
        }


        GameManager.RestartInit(_initWave, UserData.UTIIEW[_initWave].Score);


        //最大波次×0.8=可选择波次
        //记录每一关的最大分数

        return
        "选择初始波次\n<size=3>>>>>>>>>>>>>>>>>>>>>>>>>>只能10关10关选哦</size>" +
        $"\n\n\n当前选择......... <size=8>{_initWave}</size>" +
        "\n\n\n您的初始分数\n<size=3>>>>>>>>>>>>>>>>>>>>>>>>>>您在该波次的最大分数 </size>" +
        $"\n\n\n当前选择......... <size=8>{UserData.UTIIEW[_initWave].Score}</size>" +
        $"\n(在第{UserData.UTIIEW[_initWave].games_count}局达成该目标！{_comment})";



    }

    /// <summary>
    /// 等待输入效果
    /// </summary>
    /// <param name="_str"></param>
    /// <returns></returns>
    IEnumerator WaitForType(string _str)
    {
        WaitForSeconds waitForSeconds = new WaitForSeconds(0.5f);
        GetComponent<Text>().text = null;

        var _str2 = _str + "I";

        while (true)
        {
            GetComponent<Text>().text = _str;
            yield return waitForSeconds;
            GetComponent<Text>().text = _str2;
            yield return waitForSeconds;
        }
    }

    /// <summary>
    /// 打字机效果（支持富文本）
    /// </summary>
    /// <param name="_str"></param>
    /// <returns></returns>
    IEnumerator Type(string _str)
    {
        string final = null;
        string front = null;
        string back = null;

        WaitForSeconds waitForSeconds = new WaitForSeconds(0.01f);
        yield return new WaitForSeconds(2);

        StopCoroutine(CWaitForType);
        //CWaitForType = null;

        //首先永远分为前后两个文本段
        //当识别到富文本的时候，后文本段为结束文本，如“</?>”
        //当未识别到富文本标记，后文本段为""
        for (int i = 0; i < _str.ToCharArray().Length; i++)
        {
            //当遇到富文本时（这不是通用的，解决不同标记的富文本不太管用，但是暂时用不到）
            if (_str[i].Equals('<') && i + 1 < _str.ToCharArray().Length)
            {
                if (_str[i + 1].Equals('/'))
                {
                    //如果是富文本休止标记
                    int j = i;
                    //直到取到富文本结束符号，或者取到了列表的最后一个
                    while ((!_str[j].Equals('>')) && (j + 1 < _str.ToCharArray().Length))
                    {
                        front += _str[j];
                        j++;
                    }
                    front += _str[j];
                    back = "";
                    i = j;
                }
                else
                {
                    //如果是富文本开始标记
                    int j = i;
                    while ((!_str[j].Equals('>')) && (j + 1 < _str.ToCharArray().Length))
                    {
                        front += _str[j];
                        back += _str[j];
                        j++;
                    }
                    front += _str[j];

                    //是真的富文本吗，会不会只是一个>w<这样的颜文字？
                    //就从是否能索引到“=”判断是否为富文本的后缀吧！
                    if (back.ToCharArray().Contains('='))
                    {
                        back = back.Insert(1, "/");
                        back = back.Substring(0, back.IndexOf('=')) + ">";
                    }
                    else
                    {
                        back = null;
                    }
                    i = j;
                }
            }
            else
            {
                front += _str[i];
            }

            final = front + back;
            this.GetComponent<Text>().text = final;
            yield return waitForSeconds;


        }

        StopCoroutine(CWaitForType);
        CWaitForType = StartCoroutine(WaitForType(GetComponent<Text>().text));

        // final += input[i];
        // if (input[i].Equals('<'))
        // {
        //     int j = 1;
        //     while (!input[i + j].Equals('>'))
        //     {
        //         final += input[i];
        //         j++;
        //     }

        // }
        // this.GetComponent<Text>().text = final;
        // yield return waitForSeconds;
    }


    void OnMouseDrag()
    {
        switch (IUserInput.mousedrag)
        {
            case IUserInput.MOUSEDRAG.LEFT:
                if (curWave < maxWave)
                {
                    curWave += 10;
                    StartCoroutine(ShowEncryptionText(GenerateText(curWave), 0.03f, GetComponent<Text>()));
                }
                else
                {
                    curWave = maxWave;
                    UIManager.OnCommonToast("已达到能选择的最大波次");
                }
                break;
            case IUserInput.MOUSEDRAG.RIGHT:
                if (curWave > 1)
                {
                    curWave -= 10;
                    StartCoroutine(ShowEncryptionText(GenerateText(curWave), 0.03f, GetComponent<Text>()));
                }
                else
                {
                    curWave = 1;
                    UIManager.OnCommonToast("波次不能小于1哦崽");
                }
                break;
        }
    }


    /// <summary>
    /// 给玩家展现密文
    /// </summary>
    /// <param name="_text"></param>
    /// <param name="_changeTime"></param>
    /// <param name="text"></param>
    /// <returns></returns>
    IEnumerator ShowEncryptionText(string _text, float _changeTime, Text text)
    {
        WaitForSeconds waitForSeconds = new WaitForSeconds(_changeTime);
        StopCoroutine(CWaitForType);
        StopCoroutine(CType);
        // CWaitForType = null;
        // CType = null;

        var i = 1;
        var preStr = _text;
        while (i < 10)
        {
            _text = string.Join("", preStr.Select(n => Convert.ToString(n + i, 2)));
            i++;
            text.text = _text;
            yield return waitForSeconds;
        }
        text.text = preStr;
        GC.Collect();

        StopCoroutine(CWaitForType);

        CWaitForType = StartCoroutine(WaitForType(preStr));


    }

}
