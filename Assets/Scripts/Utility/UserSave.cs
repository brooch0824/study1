using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
using System.Linq;
using System;


namespace Utilities
{
    [System.Serializable]
    public class SavedUser
    {
        [SerializeField] public List<int> UserTopScore = new();//最高分（存储50）
        [SerializeField] public List<int> UserTopWave = new();//最高波次（存储50）
        [SerializeField] public List<int> UserTopPair_Score = new();//排名对的分数
        [SerializeField] public List<int> UserTopPair_Wave = new();//排名对的波次（波次和分数未必对应）

        [SerializeField] public List<int> UserTopScoreInEveryWave_Wave = new();//各波次
        [SerializeField] public List<int> UserTopScoreInEveryWave_Score = new();//各波次最大分数
        [SerializeField] public List<int> UserTopScoreInEveryWave_GamesCount = new();//各分数达成局数

        [SerializeField] public int UserGamesCount = UserData.UserGamesCount;//游玩局数
        [SerializeField] public int UserHitLuckTotalTimes = UserData.UserHitLuckTotalTimes;//撞了多少次今日好运+1
        [SerializeField] public int UserCurrDeathProtect = UserData.UserCurrDeathProtect;//还剩下几次死亡保护
        [SerializeField] public string LastLoadDate = UserData.LastLoadDate.ToString();//最后一次登录时间（这个笔记本无法存储）
        [SerializeField] public int LoadDateCount = UserData.LoadDateCount;//累计登录天数
        [SerializeField] public int HitEnemySuccessCount = UserData.HitEnemySuccessCount;//成功撞飞敌人数量

        public SavedUser()
        {
            UserTopScore = UserData.UserTopScore;
            UserTopWave = UserData.UserTopWave;
            LastLoadDate = UserData.LastLoadDate.ToString();
            LoadDateCount = UserData.LoadDateCount;
            HitEnemySuccessCount = UserData.HitEnemySuccessCount;

            SaveUserTopPair();
            SaveUserTopScoreInEveryWave();
        }

        /// <summary>
        /// 为排行榜服务，记录玩家通关时，最高的分数和波次，要求在同一关中
        /// </summary>
        void SaveUserTopPair()
        {
            //这个性能太差了，但是今天先睡觉，明天优化吧
            UserTopPair_Score.Clear();
            UserTopPair_Wave.Clear();
            UserData.UserTopPair.ForEach(n =>
            {
                UserTopPair_Score.Add(n.Score);
                UserTopPair_Wave.Add(n.Wave);
            }
            );

        }

        /// <summary>
        /// 为难度选择服务，记录玩家每一次对局中、在各关的分数最大值，不要求在同一关中，是历战结果
        /// </summary>
        void SaveUserTopScoreInEveryWave()
        {
            //波次记录成功
            UserTopScoreInEveryWave_Wave.Clear();
            UserTopScoreInEveryWave_Score.Clear();
            UserTopScoreInEveryWave_GamesCount.Clear();

            foreach (var item in UserData.UTIIEW)
            {
                UserTopScoreInEveryWave_Wave.Add(item.Key);
                UserTopScoreInEveryWave_Score.Add(item.Value.Score);
                UserTopScoreInEveryWave_GamesCount.Add(item.Value.games_count);
            }

        }

    }

    public static class UserSave
    {
        public static void SaveData()
        {
            SavedUser player = new();
            if (!Directory.Exists(Application.persistentDataPath + "/UserData"))

            {
                Directory.CreateDirectory(Application.persistentDataPath + "/UserData");
            }

            using (FileStream file = File.Create(Application.persistentDataPath + "/UserData/Default.txt"))
            {
                BinaryFormatter binaryFormatter = new();
                var json = JsonUtility.ToJson(player);
                json = string.Join("", json.Select(n => (char)(n * 2)));
                binaryFormatter.Serialize(file, json);
            }

            if (System.DateTime.Now.Date != UserData.LastLoadDate.Date)
            {

                Debug.Log($"d1 {System.DateTime.Now}");
                Debug.Log($"d2 {UserData.LastLoadDate}");
                Debug.Log(UserData.LastLoadDate == System.DateTime.Now);
                UserData.ChangeLoadDateCount(UserData.LoadDateCount + 1);
                UserData.ChangeLastLoadDate(System.DateTime.Now);
            }
        }

        public static void LoadData()
        {
            if (!File.Exists(Application.persistentDataPath + "/UserData/Default.txt")) return;
            // var json = File.ReadAllText(Application.persistentDataPath + "/UserData/Default.txt");
            SavedUser player = new();

            using (FileStream file = File.Open(Application.persistentDataPath + "/UserData/Default.txt", FileMode.Open))

            {
                BinaryFormatter formatter = new BinaryFormatter();
                string json = (string)formatter.Deserialize(file);
                json = string.Join("", json.Select(n => (char)(n / 2)));
                JsonUtility.FromJsonOverwrite(json, player);

            }
            #region 存档内容 
            UserData.UserTopScore = player.UserTopScore;//最高分（存储50）
            UserData.UserTopWave = player.UserTopWave;//最高波次（存储50）

            //你妈数据读取用了遍历不得卡死
            //好吧数据读取还好，我tm存档也用了遍历我是真牛逼
            for (int i = 0; i < player.UserTopPair_Score.Count; i++)
            {
                GameResult gameResult = new(player.UserTopPair_Score[i], player.UserTopPair_Wave[i]);
                UserData.UserTopPair.Add(gameResult);
            }


            UserData.ChangeUserGamesCount(player.UserGamesCount);//游玩局数
            UserData.ChangeUserHitLuckTotalTimes(player.UserHitLuckTotalTimes);//撞了多少次今日好运+1
            UserData.ChangeUserCurrDeathProtect(player.UserCurrDeathProtect);//还剩下几次死亡保护
            UserData.ChangeLoadDateCount(player.LoadDateCount);
            UserData.ChangeLastLoadDate(DateTime.Parse(player.LastLoadDate));
            UserData.ChangeHitEnemySuccessCount(player.HitEnemySuccessCount);


            //服务于难度选择需要知道玩家在历程中各个波次的最大分数
            int count = player.UserTopScoreInEveryWave_Wave.Count;
            for (int i = 0; i < count; i++)
            {
                UserTopInfoInEveryWave a = new()
                {
                    Score = player.UserTopScoreInEveryWave_Score[i],
                    games_count = player.UserTopScoreInEveryWave_GamesCount[i]
                };

                UserData.UTIIEW.Add(player.UserTopScoreInEveryWave_Wave[i], a);

            }
            #endregion

            Debug.Log($"d1 {System.DateTime.Now}");
            Debug.Log($"d2 {UserData.LastLoadDate}");
            if (System.DateTime.Now.Date != UserData.LastLoadDate.Date)
            {
                Debug.Log($"d1 {System.DateTime.Now}");
                Debug.Log($"d2 {UserData.LastLoadDate}");
                Debug.Log(UserData.LastLoadDate == System.DateTime.Now);
                UserData.ChangeLoadDateCount(UserData.LoadDateCount + 1);
                UserData.ChangeLastLoadDate(System.DateTime.Now);
            }
            Debug.Log("读档成功");
            UIManager.OnCommonToast("读档成功");
        }
    }
}