using System.Collections;
using UnityEngine;

namespace Utilities
{

    /// <summary>
    /// 放了通用方法
    /// </summary>
    public class Common
    {
        /// <summary>
        /// 销毁（状态机，销毁动画的名称）
        /// </summary>
        /// <param name="animator"></param>
        /// <param name="DestroyAnimName"></param>
        public static void DestroyWithAnim(Animator animator, string DestroyAnimName)
        {
            //Debug.Log("成功OnDestroy");
            animator.Play(DestroyAnimName);
        }


        /// <summary>
        /// 晃动
        /// </summary>
        public static IEnumerator Shake(Transform transform, Vector3 amplitude, float ShakeTimeSpace, float times, bool canInteract)
        {
            if (!canInteract) IUserInput.canInteract = false;
            WaitForSecondsRealtime waitTime = new WaitForSecondsRealtime(ShakeTimeSpace);
            var resetPos = transform.localPosition;
            var i = 0;
            while (i < times)
            {
                //Debug.Log($"成功执行，i:{i},Times:{times}");
                i++;
                transform.localPosition += amplitude;
                yield return waitTime;
                transform.localPosition = resetPos;
                yield return waitTime;
            }
            transform.localPosition = resetPos;
            if (!canInteract) IUserInput.canInteract = true;
        }



        /// <summary>
        /// 晃动
        /// </summary>
        public static IEnumerator Shake(RectTransform transform, Vector3 amplitude, float ShakeTimeSpace, float times, bool canInteract)
        {
            if (!canInteract) IUserInput.canInteract = false;
            WaitForSecondsRealtime waitTime = new WaitForSecondsRealtime(ShakeTimeSpace);
            var resetPos = transform.localPosition;
            var i = 0;
            while (i < times)
            {
                //Debug.Log($"成功执行，i:{i},Times:{times}");
                i++;
                transform.localPosition += amplitude;
                Debug.Log(transform.localPosition);
                yield return waitTime;
                transform.localPosition = resetPos;
                Debug.Log(transform.localPosition);
                yield return waitTime;
            }
            transform.localPosition = resetPos;
            if (!canInteract) IUserInput.canInteract = true;
        }
        /// <summary>
        /// 晃动
        /// </summary>
        public static IEnumerator Shake(Transform transform, Vector3 amplitude, float ShakeTimeSpace, float times)
        {
            WaitForSecondsRealtime waitTime = new WaitForSecondsRealtime(ShakeTimeSpace);
            var resetPos = transform.localPosition;
            var i = 0;
            while (i < times)
            {
                //Debug.Log($"成功执行，i:{i},Times:{times}");
                i++;
                transform.localPosition += amplitude;
                Debug.Log(transform.localPosition);
                yield return waitTime;
                transform.localPosition = resetPos;
                Debug.Log(transform.localPosition);
                yield return waitTime;
            }
            transform.localPosition = resetPos;
        }



        /// <summary>
        /// 晃动
        /// </summary>
        public static IEnumerator Shake(RectTransform transform, Vector3 amplitude, float ShakeTimeSpace, float times)
        {
            WaitForSecondsRealtime waitTime = new WaitForSecondsRealtime(ShakeTimeSpace);
            var resetPos = transform.localPosition;
            var i = 0;
            while (i < times)
            {
                //Debug.Log($"成功执行，i:{i},Times:{times}");
                i++;
                transform.localPosition += amplitude;
                Debug.Log(transform.localPosition);
                yield return waitTime;
                transform.localPosition = resetPos;
                Debug.Log(transform.localPosition);
                yield return waitTime;
            }
            transform.localPosition = resetPos;
        }






        /// <summary>
        /// 插值平滑旋转
        /// </summary>
        /// <param name="transform"></param>
        /// <param name="Init"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public static IEnumerator SmoothRotate(Transform transform, float Init, float target)
        {
            IUserInput.canInteract = false;
            float total = Init;
            float x = 100;
            while (Mathf.Abs(x) > 0.003f/*target - total > 0.01f*/)
            {
                x = (target - total) * Time.deltaTime * 5f;
                transform.Rotate(Vector3.up, x);
                total += x;
                if (Mathf.Abs(x) > 0.1f)
                    IUserInput.canInteract = true;
                yield return null;
            }
            transform.Rotate(Vector3.up, target - total);
            // while (Mathf.Absd
        }


        /// <summary>
        /// 使3D物体大小适配屏幕
        /// </summary>
        /// <param name="trans"></param>
        public static void FitScreen(Transform trans, CommonFitScreen.FITTYPE fITTYPE)
        {
            switch (fITTYPE)
            {
                case CommonFitScreen.FITTYPE.SMALL:
                    float f = (float)Screen.width / Screen.height;
                    trans.localScale *= f * 16f / 9f;
                    break;
                case CommonFitScreen.FITTYPE.BIG:
                    float multi = Screen.width / 9f >= Screen.height / 16f ? Screen.width / 1080f : Screen.height / 1920f;
                    trans.localScale *= multi;
                    break;
            }
        }

        // /// <summary>
        // /// 使sprite适配屏幕位置
        // /// </summary>
        // /// <param name="trans"></param>
        // public static void FitScreen(Transform trans, Vector2 vector2)
        // {
        //     // trans.localPosition = new Vector2(Screen.width * 0.5f * vector2.x, Screen.width * 0.5f * vector2.y);

        //     // float multiX = Screen.width / 1080f;
        //     // float multiY = Screen.height / 1920f;
        //     // Vector3 leftEdgeWorldPosition = Camera.main.ScreenToWorldPoint(new Vector3(0, vector2.x * Screen.eight, Camera.main.nearClipPlane));

        //     // float EdgeX = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width * (0.5f * vector2.x + 0.5f), 0, Camera.main.nearClipPlane)).x;
        //     // float EdgeY = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height * (0.5f * vector2.y + 0.5f), Camera.main.nearClipPlane)).y;
        //     // Debug.Log(EdgeX);



        //     float newX = vector2.x * Screen.width / 1080f + (trans.localPosition.x - vector2.x);
        //     float newY = vector2.y * Screen.height / 1920f + (trans.localPosition.y - vector2.y);
        //     // float newY = EdgeY + (EdgeY * 1920f / Screen.width - trans.localPosition.y);
        //     //float newY = vector2.y * Screen.height * 0.5f + (vector2.y * 1920 / Screen.height - trans.localPosition.y);
        //     //trans.localPosition = new Vector3(trans.localPosition.x * multiX, trans.localPosition.y * multiY, trans.localPosition.z);
        //     trans.localPosition = new Vector3(newX, newY, trans.localPosition.z);
        // }




    }

}