using System;

public class GameResult : IComparable<GameResult>
{
    public int Score { get; }
    public int Wave { get; }


    public GameResult(int _score, int _wave)
    {
        Score = _score;
        Wave = _wave;
    }

    public int CompareTo(GameResult other)
    {
        if (other == null)
        {
            return 1;
        }
        if (other.Score == Score)
        {
            return Wave - other.Wave;
        }
        else
        {
            return Score - other.Score;
        }
    }
}
