using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommonFitScreen : MonoBehaviour
{
    public enum FITTYPE
    {
        [SerializeField, Label("缩小")] SMALL,
        [SerializeField, Label("拉伸")] BIG,
    }
    [SerializeField, Label("是否自适应屏幕")] public bool isFittype = true;
    [SerializeField, Label("自适应屏幕")] public FITTYPE fittype = FITTYPE.SMALL;
    // [SerializeField, Label("是否对齐")] public bool isAdapter;
    // [SerializeField,Label("对齐坐标点")] public Vector2 adapter;

    void Start()
    {
        if (isFittype)
        {
            Utilities.Common.FitScreen(transform, fittype);
        }
        // if (isAdapter)
        // {
        //     Utilities.Common.FitScreen(transform, adapter);
        // }
    }

}
