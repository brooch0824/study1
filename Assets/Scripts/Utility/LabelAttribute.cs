//第一个建议存放目录：Assets/Scripts/Utility/LabelAttribute.cs
//注意这个LabelAttribute.cs千万不要放在Editor文件夹下，否则会报错找不到Label("姓名")这个特性。
using UnityEngine;
public class LabelAttribute:PropertyAttribute
{
  private string name = "";   
  public string Name { get { return name; } }
  public LabelAttribute(string name)
  {
    this.name = name;
  }
}