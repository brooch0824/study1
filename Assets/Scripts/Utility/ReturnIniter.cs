using UnityEngine;

public class ReturnIniter : MonoBehaviour
{
    public GameObject m_InitObject;
    public Transform m_InitPath;
    public Vector3 m_InitPos;
    void Start()
    {

        foreach (Transform child in m_InitPath)
        {
            Destroy(child.gameObject);
        }
        var returnObj = Instantiate(m_InitObject, m_InitPos, Quaternion.Euler(0, 0, 0), m_InitPath);
        returnObj.GetComponent<MainHallCubeTrigger>().m_ENname = "ReturnMainHall";
        returnObj.GetComponent<MainHallCubeTrigger>().m_CNname = "返回";
        returnObj.GetComponent<MainHallCubeTrigger>().canScaleChangedByDis = false;
    }
}