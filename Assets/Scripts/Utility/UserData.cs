using System;
using System.Collections.Generic;
using UnityEngine;

namespace Utilities
{
    public class UserTopInfoInEveryWave
    {
        public int Score;//该波次分数
        public int games_count;//第几局

    }


    public class UserData
    {
        public static List<ADAwards> ADAwardsList = new();
        public static List<GameObject> MainAreaPrefabList = new();
        public static List<int> UserTopScore = new();//最高分（存储100）
        public static List<int> UserTopWave = new();//最高波次（存储100）
                                                    // public static Dictionary<int, int> UserTopScoreWavePair = new Dictionary<int, int>();//最高积分+波次对（存储100）
        public static List<GameResult> UserTopPair = new();
        public static Dictionary<int, UserTopInfoInEveryWave> UTIIEW = new();//玩家各波次的巅峰状态，波次为主键

        public static int UserGamesCount { get; private set; }//看玩家玩了多少局
        public static int UserHitLuckTotalTimes { get; private set; }//撞了多少次今日好运+1
        public static int UserCurrDeathProtect { get; private set; }//还剩下几次死亡保护
        public static DateTime LastLoadDate { get; private set; }//最后一次登录日期
        public static int LoadDateCount { get; private set; }//累计登录天数
        public static int HitEnemySuccessCount { get; private set; }//成功撞飞敌人的数量



        /// <summary>
        /// 读取资源
        /// </summary>
        public static void ResourcesLoad()
        {
            // //不知道这步有没有用先把Prefabs都加载了（？
            // MainAreaPrefabList.Clear();
            // MainAreaPrefabList.AddRange(Resources.LoadAll<GameObject>("Prefabs"));

            ADAwardsList.Clear();
            ADAwardsList.AddRange(Resources.LoadAll<ADAwards>("ScriptObjects/ADs"));
            if (ADAwardsList.Count == 0)
                Debug.LogError("资源加载错误！检查");





            Debug.Log("资源加载完成");
        }

        /// <summary>
        /// 结算时传入积分波次对
        /// </summary>
        public static void TryChangeTopScoreWavePair(GameResult gameResult)
        {
            //最高得分单独存储
            //最高波次单独存储
            //最高得分时的波次单独存储，这个用于显示在排行榜上
            UserTopScore.Add(gameResult.Score);
            UserTopScore.Sort();
            UserTopScore.Reverse();
            if (UserTopScore.Count > 100)
            {
                UserTopScore.RemoveAt(100);
            }



            UserTopWave.Add(gameResult.Wave);
            UserTopWave.Sort();
            UserTopWave.Reverse();
            if (UserTopWave.Count > 100)
            {
                UserTopWave.RemoveAt(100);
            }

            UserTopPair.Add(gameResult);
            UserTopPair.Sort();
            UserTopPair.Reverse();
            if (UserTopPair.Count > 100)
            {
                UserTopPair.RemoveAt(100);
            }
            var a = 1;
            UserTopPair.ForEach(n =>
            {
                Debug.Log($"第{a}名的分数：{n.Score},波次：{n.Wave},列表长度：{UserTopPair.Count}");
                a++;
            }
            );


            // if (UserTopScoreWavePair.ContainsKey(score))
            // {
            //     UserTopScoreWavePair[score] = UserTopScoreWavePair[score] > wave ? UserTopScoreWavePair[score] : wave;
            // }
            // else
            // {
            //     UserTopScoreWavePair.Add(score, wave);
            // }

            // UserTopScoreWavePair = UserTopScoreWavePair.OrderBy(kv => kv.Key).ToDictionary(kv => kv.Key, ky => ky.Value);
            // if (UserTopScoreWavePair.Count > 100)
            // {
            //     UserTopScoreWavePair.Remove(UserTopScore[100]);
            // }
            // var a = 1;
            // foreach (var x in UserTopScoreWavePair)
            // {
            //     Debug.Log($"第一名的分数：{x.Key},波次：{x.Value},字典长度：{UserTopScoreWavePair.Count}");
            //     a++;
            // }
        }

        /// <summary>
        /// 改变玩家死亡保护次数
        /// </summary>
        /// <param name="changeNum"></param>
        public static void ChangeUserCurrDeathProtect(int changeNum)
        {
            foreach (var x in ADAwardsList)
            {
                if (x.aDAWARD == ADAwards.ADAWARD.DEATHPROTECT)
                {
                    UserCurrDeathProtect = Mathf.Clamp(changeNum, 0, x.InventoryLimit);
                    UserSave.SaveData();
                    return;
                }
            }
        }

        /// <summary>
        /// 改变玩家累计撞击Luck的次数
        /// </summary>
        /// <param name="changeNum"></param>
        public static void ChangeUserHitLuckTotalTimes(int changeNum)
        {
            UserHitLuckTotalTimes = changeNum;
            UserSave.SaveData();
        }

        /// <summary>
        /// 改变玩家每局游玩次数
        /// </summary>
        /// <param name="changeNum"></param>
        public static void ChangeUserGamesCount(int changeNum)
        {
            UserGamesCount = changeNum;
        }

        /// <summary>
        /// 改变登录天数
        /// </summary>
        /// <param name="changeNum"></param>
        public static void ChangeLoadDateCount(int changeNum)
        {
            LoadDateCount = changeNum;
            UserSave.SaveData();
        }

        /// <summary>
        /// 改变最后一天登录时间
        /// </summary>
        /// <param name="changeNum"></param>
        public static void ChangeLastLoadDate(DateTime changeNum)
        {
            LastLoadDate = changeNum;
            UserSave.SaveData();
        }


        /// <summary>
        /// 改变撞飞的敌人的数量
        /// </summary>
        /// <param name="changeNum"></param>
        public static void ChangeHitEnemySuccessCount(int changeNum)
        {
            HitEnemySuccessCount = changeNum;
            UserSave.SaveData();
        }


        /// <summary>
        /// 改变玩家每波的最高分
        /// </summary>
        /// <param name="_currentWave"></param>
        /// <param name="_currentScore"></param>
        public static void ChangeUTIIEW(int _currentWave, int _currentScore)
        {

            //当前分数更高就存储更高的，如果不存在就记录
            //最多存储200个键值对
            if (_currentWave > 200 || _currentWave < 1) return;

            if (UTIIEW.ContainsKey(_currentWave))
            {
                int _maxScore = UTIIEW[_currentWave].Score;

                if (_maxScore < _currentScore)
                {
                    UTIIEW[_currentWave].Score = _currentScore;
                    UTIIEW[_currentWave].games_count = UserGamesCount + 1;
                    //UserSave.SaveData(); 不用存档，结算统一存
                }

            }
            else
            {
                UserTopInfoInEveryWave a = new();
                a.Score = _currentScore;
                a.games_count = UserGamesCount + 1;

                UTIIEW.Add(_currentWave, a);
            }
        }

    }
}

