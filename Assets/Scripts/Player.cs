using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public Animator animator;
    public GameObject info;
    bool isCheckingIdle;
    Text Num;
    float number;
    float idleTime = 0;
    float idleTargetTime = 3;
    Material material;

    void Awake()
    {
        Num = transform.Find("Canvas/Num").GetComponent<Text>();
        animator = GetComponent<Animator>();
        material = transform.Find("Body").GetComponent<MeshRenderer>().material;
        //animator?.Play("Player_Init");
        //transform.Find("Body").GetComponent<MeshRenderer>().material.SetFloat("_ClipThreshold", -1);
    }
    // void OnTriggerEnter()
    // {
    //     Debug.Log("主体Trigger碰撞成功");
    // }
    // void OnCollisionEnter()
    // {
    //     Debug.Log("主体Collider碰撞成功");
    // }

    void OnEnable()
    {
        IUserInput.m_OnMouseDrag += PlayerOnMouseDrag;
        LoadManager.m_onGameInfo += OnGameInfo;

    }
    void OnDisable()
    {
        IUserInput.m_OnMouseDrag -= PlayerOnMouseDrag;
        LoadManager.m_onGameInfo -= OnGameInfo;
    }

    void Start()
    {
        // RefreshNum();
        number = 0;
        material.SetFloat("_ClipThreshold", 1f);
        info.gameObject.SetActive(false);
    }

    void Update()
    {
        //PlayerOnMouseDrag();
        number = Mathf.Lerp(number, GameManager.UserNum, 0.2f);
        material.SetFloat("_ClipThreshold", Mathf.Lerp(material.GetFloat("_ClipThreshold"), -4f, Time.deltaTime));
        Num.text = number.ToString("f0");
        //Debug.Log(mousedrag + " in Player");
        // if (Convert.ToInt32(Num.text) != Utilities.Utility.UserNum)
        // {
        // }
    }


    void OnGameInfo()
    {
        info.SetActive(true);
        Debug.Log("OnGameInfo");
    }

    // void RefreshNum()
    // {
    //     StartCoroutine(RefreshNumCo());
    // }

    // IEnumerator RefreshNumCo()
    // {
    //     while (Utilities.Utility.UserNum - Convert.ToInt32(Num.text) > 1)
    //     {
    //         yield return null;
    //     }
    //     Debug.Log("RefreshNumCo");
    // }

    /// <summary>
    /// 决定玩家会播哪个动画
    /// </summary> <summary>
    void PlayerOnMouseDrag()
    {
        // Debug.Log(IUserInput.mousedrag);
        //检测拖动
        if (IUserInput.mousedrag != IUserInput.MOUSEDRAG.NULL)
        {
            //StopCoroutine(CheckIdle());
            //如果待机的时候进行了拖动，立刻进入游戏状态
            var isOncall = GameManager.gAMESTATE == GameManager.GAMESTATE.GAMEONCALL;
            var isGaming = GameManager.gAMESTATE == GameManager.GAMESTATE.GAMING ? true : false;
            var isMainHall = GameManager.gAMESTATE == GameManager.GAMESTATE.MAINHALL ? true : false;
            var isPrepare = GameManager.gAMESTATE == GameManager.GAMESTATE.PREPARE ? true : false;

            if (isOncall) LoadManager.Gaming();

            idleTime = 0;
            Time.timeScale = 1;
            if (animator != null)
            {
                switch (IUserInput.mousedrag)
                {
                    case IUserInput.MOUSEDRAG.UP:
                        animator.SetTrigger("DragUp");
                        break;
                    case IUserInput.MOUSEDRAG.DOWN:
                        animator.SetTrigger("DragDown");
                        break;
                    case IUserInput.MOUSEDRAG.LEFT:
                        if (isGaming || isOncall)
                        {
                            animator.SetTrigger("DragLeft");
                        }
                        else
                        {
                            animator.Play("Player_MainHall_Left");
                        }
                        break;
                    case IUserInput.MOUSEDRAG.RIGHT:
                        if (isGaming || isOncall)
                        {
                            animator.SetTrigger("DragRight");
                        }
                        else
                        {
                            animator.Play("Player_MainHall_Right");
                        }
                        break;
                    default:
                        Debug.LogError("没有输入");
                        break;
                }
            }
        }
        //Debug.Log(animator.GetCurrentAnimatorStateInfo(0).IsName("Player_Idle"));
        // //Idle下等待3秒进入随机Idle
        // if (!isCheckingIdle && animator.GetCurrentAnimatorStateInfo(0).IsName("Player_Idle"))
        // {
        //     //StartCoroutine(CheckIdle());
        // }
    }

    /// <summary>
    /// 检测玩家应该在哪个待机动画
    /// </summary>
    void CheckIdle()
    {
        if (isCheckingIdle == false)
        {
            idleTargetTime = Random.Range(3, 10);
        }
        isCheckingIdle = true;
        //Debug.Log("idleTime" + idleTime);
        idleTime += Time.deltaTime;
        if (idleTime >= idleTargetTime && animator.GetCurrentAnimatorStateInfo(0).IsName("Player_Idle"))
        {
            var y = Random.Range(0, 2);
            if (y < 1)
            {
                animator.Play("Player_Idle_2");
            }
            else
            {
                animator.Play("Player_Idle_3");
            }
            animator.SetLayerWeight(0, 0);
            animator.SetLayerWeight(1, 1);
            isCheckingIdle = false;
            idleTime = 0;
        }
        //StopCoroutine(CheckIdle());
    }

    public IEnumerator ResetBody(string path)
    {
        GameObject body = transform.Find(path).gameObject;
        float speed = 0.5f;
        while (body.transform.localPosition.magnitude > 0.01f)
        {
            body.transform.localPosition = Vector3.Slerp(body.transform.localPosition, Vector3.zero, speed);
            body.GetComponent<Rigidbody>().velocity = Vector3.zero;
            body.transform.localRotation = Quaternion.Euler(0, 0, 0);
            yield return null;
        }
    }

    // /// <summary>
    // /// 重新生成玩家数字
    // /// </summary>
    // public void RegenerateUserNum()
    // {

    //     Utilities.Utility.RegistUserNum();
    // }

    void OnEyesIdleEnter()
    {
        animator.SetLayerWeight(animator.GetLayerIndex("Base Layer"), 1);
        animator.SetLayerWeight(animator.GetLayerIndex("Eyes"), 0);
    }


    void ResetTrigger()
    {
        animator.ResetTrigger("DragUp");
        animator.ResetTrigger("DragDown");
        animator.ResetTrigger("DragLeft");
        animator.ResetTrigger("DragRight");
    }
    // void OnEyesIdleExit()
    // {
    //     animator.SetLayerWeight(animator.GetLayerIndex("Base Layer"), 0);
    //     animator.SetLayerWeight(animator.GetLayerIndex("Eyes"), 1);
    // }

}
