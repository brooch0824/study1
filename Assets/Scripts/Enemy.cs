using System;
using System.Collections;
using UnityEngine;
using Utilities;

public class Enemy : MonoBehaviour
{

    public enum ENEMYTYPE
    {
        NULL,
        INVISIBLE,
        FADE
    }
    public ENEMYTYPE eNEMYTYPE = ENEMYTYPE.NULL;
    delegate void OnTrigger();
    event OnTrigger m_OnTrigger;

    public float speedDuringFlyingAway = 0.1f;
    public int num;
    public GameManager gameManager;
    public Animator animator;
    Vector3 oldPos;


    void OnEnable()
    {
        m_OnTrigger += OnEnemyTriggerEnter;
    }
    void OnDisable()
    {
        m_OnTrigger -= OnEnemyTriggerEnter;
    }

    void Start()
    {
        animator = transform.GetComponent<Animator>();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        oldPos = transform.position;
        transform.position *= 3;
        WhileCreate();
    }

    // public void ReadyToDestroy()
    // {
    //     //Debug.Log("成功OnDestroy");
    //     animator.Play("Enemy_Destroy");
    // }

    public void OnPlayerTrigger()
    {

        m_OnTrigger();
    }


    void DestroyThis()
    {
        Destroy(this.gameObject);
    }

    void OnEnemyTriggerEnter()
    {
        // if (child.GetComponent<EnemyTrigger>().isTriggerThis)
        //{
        //Debug.Log("gameManager.IsPlayerBiggerThanOther(num)" + gameManager.IsPlayerBiggerThanOther(num));
        //Debug.Log("receive OnEnemyTriggerEnter");
        if (gameManager.IsPlayerBiggerThanOther(num))
        {
            //Debug.Log($"碰撞成功，玩家数字{Utilities.Utility.UserNum},方块数字{num}");
            StartCoroutine(FlyAway());
            UserData.ChangeHitEnemySuccessCount(UserData.HitEnemySuccessCount + 1);
            StartCoroutine(CheckNewWaveInGameManager());

        }
        else
        {
            animator.Play("Enemy_Stay");
            gameManager.CheckIfGameOver();
            GameManager.gAMEOVERTYPE = GameManager.GAMEOVERTYPE.WRONGHIT;
        }
        //  }

    }


    /// <summary>
    /// 被创建时执行
    /// </summary>
    /// <returns></returns>
    void WhileCreate()
    {
        StartCoroutine(WhileCreateMove());
        StartCoroutine(WhileCreateNum());
    }

    IEnumerator WhileCreateMove()
    {
        // 移动到目标位置
        while ((transform.position - oldPos).magnitude > 0.01f)
        {
            //Debug.Log($"WhileCreate,now Mag{(transform.position - oldPos).magnitude}");
            transform.position = Vector3.Slerp(transform.position, oldPos, 0.04f);
            yield return null;
        }
    }

    IEnumerator WhileCreateNum()
    {
        switch (eNEMYTYPE)
        {
            case ENEMYTYPE.NULL:
                transform.Find("Num").GetComponent<TextMesh>().text = num.ToString();
                break;
            case ENEMYTYPE.FADE:
                transform.Find("Num").GetComponent<TextMesh>().text = num.ToString();
                StartCoroutine(TextFade());
                break;
            case ENEMYTYPE.INVISIBLE:
                transform.Find("Num").GetComponent<TextMesh>().text = "<color=#FFCC00>?</color>";
                yield return new WaitForSeconds(0.6f);
                transform.Find("Num").GetComponent<TextMesh>().text = "<color=#FFCC00>??</color>";
                yield return new WaitForSeconds(0.1f);
                transform.Find("Num").GetComponent<TextMesh>().text = "<color=#FFCC00>???</color>";
                break;
            default:
                break;
        }
    }

    IEnumerator TextFade()
    {
        // Debug.Log("Tetxfade");
        yield return new WaitForSeconds(0.2f);
        var fade = transform.Find("Num").GetComponent<TextMesh>().color;
        while (fade.a > 0.01f)
        {
            //渐隐速度：Clamp(0.2f + 0.1f*（难度-6）,0,1)
            fade.a -= Time.deltaTime * Mathf.Clamp(-0.4f + 0.1f * GameManager.currentDiffiLv, 0, 1);
            transform.Find("Num").GetComponent<TextMesh>().color = fade;
            yield return null;
        }
        fade.a = 0;
        transform.Find("Num").GetComponent<TextMesh>().color = fade;
    }

    IEnumerator FlyAway()
    {
        Debug.Log("FlyAway~");
        //Utilities.Utility.ChangeUserNum(num);
        var old = transform.localPosition;
        while (transform.localPosition.magnitude < 20)
        {
            transform.localPosition += old * speedDuringFlyingAway;
            //Debug.Log($"增量{Time.timeScale}，执行次数看右边");
            yield return null;
        }
    }
    IEnumerator CheckNewWaveInGameManager()
    {
        yield return new WaitForSeconds(0.1f);
        gameManager.RemoveNumList(num);
    }
}
