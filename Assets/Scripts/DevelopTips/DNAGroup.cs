using UnityEngine;
using UnityEngine.UIElements;


public class DNAGroup : MonoBehaviour
{
    [SerializeField, Label("空间间隔Y")] public float m_spacing;
    [SerializeField, Label("两个之间的旋转差距")] public float m_rotate;
    [SerializeField, Label("平着的时候持续几根不动")] public int m_quantity;
    [SerializeField, Label("自转速度")][Range(1, 100)] public int m_selfRotateSpeed;

    void OnEnable()
    {
        //Utilities.Common.FitScreen(transform);

        Transform baseOne = null;
        Debug.Log("OnDrawGizmos");
        var a = 0;
        foreach (Transform child in transform)
        {
            if (a == 0) baseOne = child.transform;
            child.transform.position = a * m_spacing * Vector3.up + baseOne.position;
            child.transform.eulerAngles = a * m_rotate * Vector3.up + baseOne.eulerAngles;

            StartCoroutine(Utilities.Common.SmoothRotate(child.transform, child.transform.eulerAngles.y, -child.transform.eulerAngles.y));


            a++;
        }

    }

    void Update()
    {
        transform.Rotate(Vector3.up, m_selfRotateSpeed * Time.deltaTime);
    }
}
