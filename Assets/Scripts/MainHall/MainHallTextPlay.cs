using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainHallTextPlay : MonoBehaviour
{
    public Transform m_otherTrans;
    public float multi = 1;
    float _elapsed;

    // Start is called before the first frame update
    void Start()
    {
        transform.position = m_otherTrans.position + Vector3.up * multi;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += Vector3.up * Time.deltaTime;
        _elapsed += Time.deltaTime;
        if (_elapsed > 1)
        {
            Destroy(gameObject);
        }
    }
}
