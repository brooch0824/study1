using UnityEngine;

public class IUserInput : MonoBehaviour
{
    public enum MOUSEDRAG
    {
        NULL,
        UP = 1,
        DOWN,
        RIGHT,
        LEFT
    }

    public static MOUSEDRAG mousedrag;
    public bool isLeftMouseDown;
    public bool isLeftMouseUp;
    public static Vector3 mouseDownPos = new Vector3();
    public delegate void OnMouseDrag();
    public static event OnMouseDrag m_OnMouseDrag;
    public static bool canInteract = true;

    // void OnEnable()
    // {
    //     m_OnMouseDrag -= IsMouseDrag;
    //     m_OnMouseDrag += IsMouseDrag;
    // }

    void Update()
    {
        isLeftMouseDown = Input.GetMouseButtonDown(0);
        isLeftMouseUp = Input.GetMouseButtonUp(0);
        if (canInteract)
        {
            if (GameManager.gAMESTATE == GameManager.GAMESTATE.INFO)//在查看信息时点击任意位置回到主界面
            {
                if (isLeftMouseUp || Input.anyKeyDown)
                {
                    LoadManager.GameMainHall();
                    //GameObject.FindWithTag("Player").transform.Find("Info").gameObject.SetActive(false);
                }
            }

            if (isLeftMouseDown) mouseDownPos = Input.mousePosition;
            if (isLeftMouseUp || Input.anyKeyDown)
            {
                if (isLeftMouseUp) IsMouseDrag();
                if (Input.anyKey) IsKeyCodeDown();
                
                if (mousedrag != MOUSEDRAG.NULL)
                {
                    m_OnMouseDrag();
                    ResetMouseDrag();
                }
                //Debug.Log(mousedrag + " in IUserInput");
            }
        }
        else
        {
            mousedrag = MOUSEDRAG.NULL;
        }
    }

    void IsKeyCodeDown()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow)) mousedrag = MOUSEDRAG.LEFT;
        if (Input.GetKeyDown(KeyCode.RightArrow)) mousedrag = MOUSEDRAG.RIGHT;
        if (Input.GetKeyDown(KeyCode.UpArrow)) mousedrag = MOUSEDRAG.UP;
        if (Input.GetKeyDown(KeyCode.DownArrow)) mousedrag = MOUSEDRAG.DOWN;
    }


    void IsMouseDrag()
    {
        //首先判断是否发生了滑动
        //如果是，继续；如果否，返回false
        //然后判断左右滑动范围和上下滑动范围，谁大取谁
        var dragDis = Input.mousePosition - mouseDownPos;
        var disX = dragDis.x;
        var disY = dragDis.y;

        //Debug.Log("mag:" + dragDis.magnitude);
        if (dragDis.magnitude < 150) return;


        bool isLeft = disX < 0 && (Mathf.Abs(disX) > Mathf.Abs(disY));
        bool isRight = disX > 0 && (disX > Mathf.Abs(disY));
        bool isUp = disY > 0 && (Mathf.Abs(disX) < disY);
        bool isDown = disY < 0 && (Mathf.Abs(disX) < Mathf.Abs(disY));



        if (isLeft)
        {
            mousedrag = MOUSEDRAG.LEFT;
        }
        else if (isRight)
        {
            mousedrag = MOUSEDRAG.RIGHT;
        }
        else if (isUp)
        {
            mousedrag = MOUSEDRAG.UP;
        }
        else if (isDown)
        {
            mousedrag = MOUSEDRAG.DOWN;
        }
        else
        {
            Debug.LogError("无法判断方向，检查逻辑");
            return;
        }
        return;
    }

    protected void ResetMouseDrag()
    {
        mousedrag = MOUSEDRAG.NULL;
    }

}
