using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(menuName = "工具/广告奖励类型", fileName = "ADAward_{0}")]
public class ADAwards : ScriptableObject
{
    public enum ADAWARD
    {
        [SerializeField, Label("死亡保护")] DEATHPROTECT,//死亡保护
        [SerializeField, Label("双倍积分")] DOUBLESCORE,//双倍积分
        [SerializeField, Label("随机奖励")] RANDOM
    }

    [SerializeField, Label("广告类型")] public ADAWARD aDAWARD;
    [SerializeField, Label("堆叠上限")] public int InventoryLimit;//堆叠上限
}
