using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class CommonRotate : MonoBehaviour
{
    public Vector3 axis;
    public float speed;
    void Update()
    {
        this.transform.Rotate(axis, speed * Time.deltaTime);
    }
}
