using System;
using UnityEngine;

public class LoadManager : MonoBehaviour
{
    public delegate void OnGameStateEnter();
    public static event OnGameStateEnter m_onGameOncall;//待机时触发的事件
    public static event OnGameStateEnter m_onGameMainHall;//进入大厅时触发的事件
    public static event OnGameStateEnter m_onGaming;//进入大厅时触发的事件
    public static event OnGameStateEnter m_onGameOver;//进入游戏结束时触发的事件
    public static event OnGameStateEnter m_onGamePrepare;//进入战前准备时触发的事件
    public static event OnGameStateEnter m_onGameRank;//进入排行榜时触发的事件
    public static event OnGameStateEnter m_onGameDevelopment;//进入养成时触发的事件
    public static event OnGameStateEnter m_onGameDiffSelect;//进入难度选择时触发的事件
    public static event OnGameStateEnter m_onGameInfo;//进入玩家详情时


    void Start()
    {
        GameMainHall();
    }


    /// <summary>
    /// 游戏结束
    /// </summary>
    public static void GameOver()
    {
        //GC回收
        //GC.Collect();
        GameManager.gAMESTATE = GameManager.GAMESTATE.GAMEOVER;
        m_onGameOver();
    }

    /// <summary>
    /// 战前准备
    /// </summary>
    public static void GamePrepare()
    {
        //GC回收
        GameManager.gAMESTATE = GameManager.GAMESTATE.PREPARE;
        m_onGamePrepare();
        GC.Collect();
    }

    /// <summary>
    /// 战斗
    /// </summary>
    public static void Gaming()
    {
        //GC回收
        //GC.Collect();
        GameManager.gAMESTATE = GameManager.GAMESTATE.GAMING;
        m_onGaming();
    }


    /// <summary>
    /// 回到大厅触发的事件
    /// </summary>
    public static void GameMainHall()
    {
        GameManager.gAMESTATE = GameManager.GAMESTATE.MAINHALL;

        m_onGameMainHall();
        //GC回收
        GC.Collect();
    }

    /// <summary>
    /// 进入游戏界面待机
    /// </summary>
    public static void GameOncall()
    {
        //GC回收
        //GC.Collect();
        GameManager.gAMESTATE = GameManager.GAMESTATE.GAMEONCALL;
        m_onGameOncall();
        // foreach (var x in m_onGameOncall.GetInvocationList())
        // {
        //     Debug.Log(x);
        // }
    }
    /// <summary>
    /// 看排行榜时
    /// /// </summary>
    public static void GameRank()
    {
        //GC回收
        //GC.Collect();
        GameManager.gAMESTATE = GameManager.GAMESTATE.RANK;
        m_onGameRank();
    }

    /// <summary>
    /// 看养成时
    /// /// </summary>
    public static void GameDevelopment()
    {
        //GC回收
        //GC.Collect();
        GameManager.gAMESTATE = GameManager.GAMESTATE.DEV;
        m_onGameDevelopment();
    }


    /// <summary>
    /// 看难度选择时
    /// /// </summary>
    public static void GameDiffSelect()
    {
        //GC回收
        //GC.Collect();
        GameManager.gAMESTATE = GameManager.GAMESTATE.DIFFSELECT;
        m_onGameDiffSelect();
    }

    /// <summary>
    /// 看玩家信息
    /// /// </summary>
    public static void GameInfo()
    {
        //GC回收
        //GC.Collect();
        GameManager.gAMESTATE = GameManager.GAMESTATE.INFO;
        m_onGameInfo();
    }


}
