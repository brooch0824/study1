using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeMgr : MonoBehaviour
{
    //这个脚本的任务是在被生成后，根据sprite的四条边均匀生成方块

    [Range(1, 10)] public float m_range;
    public int count;
    public GameObject cube;
    //public GameObject m_escapeTarget;
    public Transform m_initPath;
    public float m_resetMultiple;
    public Transform m_boundTrans;
    List<Vector2> pointList = new List<Vector2>();

    void Start()
    {
        m_boundTrans.localScale = Vector3.one * m_range;
        GenerateCube(m_boundTrans, count, m_resetMultiple);
        //Debug.Log(m_resetMultiple);
    }

    void Update()
    {
        //m_boundTrans.localScale = Vector3.one * m_range;
        //RefreshPointList(transform);
        //m_range += 5 * Time.deltaTime;
    }

    void RefreshPointList(Transform trans)
    {
        if (pointList.Count == 5)
        {
            pointList[0] = new Vector2(-0.5f * trans.localScale.x, -0.5f * trans.localScale.y);
            pointList[1] = new Vector2(-0.5f * trans.localScale.x, 0.5f * trans.localScale.y);
            pointList[2] = new Vector2(0.5f * trans.localScale.x, 0.5f * trans.localScale.y);
            pointList[3] = new Vector2(0.5f * trans.localScale.x, -0.5f * trans.localScale.y);
            pointList[4] = new Vector2(-0.5f * trans.localScale.x, -0.5f * trans.localScale.y);
        }
        Debug.Log("RefreshPointList");
    }


    /// <summary>
    /// 生成小Cube（根据目标Transform的每个边均匀生成）
    /// </summary>
    /// <param name="boundTrans"></param>
    /// <param name="perSideCount"></param>
    /// <param name="targetScaleMulti"></param>
    void GenerateCube(Transform boundTrans, int perSideCount, float targetScaleMulti)
    {
        //获得四个端点的位置
        //每两个端点做差取向量
        //每段向量约分，从1->每边数量个，最后一个不取
        pointList.Clear();
        pointList.Add(new Vector2(-0.5f * boundTrans.localScale.x, -0.5f * boundTrans.localScale.y));
        pointList.Add(new Vector2(-0.5f * boundTrans.localScale.x, 0.5f * boundTrans.localScale.y));
        pointList.Add(new Vector2(0.5f * boundTrans.localScale.x, 0.5f * boundTrans.localScale.y));
        pointList.Add(new Vector2(0.5f * boundTrans.localScale.x, -0.5f * boundTrans.localScale.y));
        pointList.Add(new Vector2(-0.5f * boundTrans.localScale.x, -0.5f * boundTrans.localScale.y));
        //Debug.Log(pointList[0] + " " + trans.localScale.x);


        //每边生成perSideCount个正方体
        //每两个顶点生成一次
        for (int j = 0; j < pointList.Count - 1; j++)
        {
            //disVec等于两个顶点的向量差
            Vector2 disVec = pointList[j + 1] - pointList[j];
            //第n个*向量差/总切分段数+减数即坐标
            for (int i = 0; i < perSideCount; i++)
            {
                Vector2 nowPos = i * disVec / perSideCount + pointList[j];
                GameObject cubeObj = Instantiate(
                    cube,
                    new Vector3(
                        nowPos.x,
                        nowPos.y,
                        0),
                        Quaternion.Euler(0, 0, 0),
                        m_initPath);
                //Debug.Log(cubeObj.transform.position);
                //cubeObj.transform.localScale *= targetScaleMulti;
                //Debug.Log("targetScaleMulti:" + targetScaleMulti);
                //StartCoroutine(BindingPoint(cubeObj, perSideCount, i, j));
                //StartCoroutine(ResetScale(cubeObj, m_resetMultiple));
            }

        }
    }


    // /// <summary>
    // /// 根据原有的比例继续占据原有的位置
    // /// 很好用，但是用不到，哭哭
    // /// </summary>
    // /// <param name="who"></param>
    // /// <param name="perSideCount"></param>
    // /// <param name="whichPart"></param>
    // /// <param name="whichLine"></param>
    // /// <returns></returns>
    // IEnumerator BindingPoint(GameObject who, int perSideCount, int whichPart, int whichLine)
    // {
    //     Debug.Log("BindingPoint");
    //     //本方法默认在Update会更新新的坐标点（相当于一个字典了）
    //     //没写完，先试一下另外一个
    //     //只要自己还在就一直绑定坐标并且更新坐标
    //     while (who.activeSelf == true)
    //     {
    //         Vector2 disVec = pointList[whichLine + 1] - pointList[whichLine];
    //         Vector2 nowPos = whichPart * disVec / perSideCount + pointList[whichLine];
    //         who.transform.position = new Vector3(nowPos.x, nowPos.y, -1);
    //         yield return null;
    //     }
    // }


    // /// <summary>
    // /// 从目标点逃离（反方向）(形状不是个方形，暂时用不到了)
    // /// </summary>
    // /// <returns></returns>
    // IEnumerator EscapeFromPoint(GameObject escapeUnit, GameObject escapeTarget, float speedMulti)
    // {
    //     var Vec = escapeUnit.transform.position - escapeTarget.transform.position;
    //     //int a = 0;//这个可能有问题，协程第一行声明的变量不知道有没有效果
    //     while (Vec.magnitude < 100)
    //     {
    //         escapeUnit.transform.position += Vec.normalized * Time.deltaTime * speedMulti;
    //         yield return null;
    //     }
    // }




    // /// <summary>
    // /// 不知道怎么获得物品的世界scale
    // /// </summary>
    // /// <param name="who"></param>
    // /// <returns></returns>
    // IEnumerator ResetScale(GameObject who, float multi)
    // {
    //     while (who.activeSelf)
    //     {
    //         who.transform.localScale = new Vector3(who.transform.localScale.x / who.transform.lossyScale.x, who.transform.localScale.y / who.transform.lossyScale.y, who.transform.localScale.z / who.transform.lossyScale.z) * multi;
    //         yield return null;
    //     }
    // }

}