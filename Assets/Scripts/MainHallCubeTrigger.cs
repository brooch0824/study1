using System.Collections;
using System.Linq;
using UnityEngine;
using Utilities;

public class MainHallCubeTrigger : MonoBehaviour
{
    public string m_ENname;
    public string m_CNname;
    public GameObject m_TextPlayObj;
    [SerializeField, Label("随着中心点位置改变大小")] public bool canScaleChangedByDis = true;



    // void OnEnable()
    // {
    //     LoadManager.m_gameMainHall -= OnGameMainHall;
    //     LoadManager.m_gameOncall -= OnGameOncall;
    //     LoadManager.m_gameMainHall += OnGameMainHall;
    //     LoadManager.m_gameOncall += OnGameOncall;
    // }
    // void OnDisable()
    // {
    // }

    // void OnGameMainHall()
    // {
    //     ButtonList.SetActive(true);
    //     Debug.Log("Set true");
    // }
    // void OnGameOncall()
    // {
    //     ButtonList.SetActive(false);
    // }

    void Start()
    {
        if (canScaleChangedByDis) ChangeByDis();

        transform.Find("Num").GetComponent<TextMesh>().text = m_CNname;
        if (m_ENname.Equals("Luck"))
        {
            GetComponent<MeshRenderer>().material.SetTexture("_MainTex", Resources.Load<Texture>("Images/Material_Brick"));
        }
        StartCoroutine(Appear());
    }

    IEnumerator Appear()
    {
        Material material = GetComponent<MeshRenderer>().material;
        float x = 1;
        while (x > -4)
        {
            x -= Time.deltaTime * 2;
            material.SetFloat("_ClipThreshold", x);
            yield return null;
        }
        material.SetFloat("_ClipThreshold", -4);

    }

    void Update()
    {
        //离中心点越远的单位大小越小
        if (canScaleChangedByDis) ChangeByDis();
    }

    void ChangeByDis()
    {
        transform.localScale = Vector3.one * (1 - Mathf.Min(0.2f * Mathf.Abs(transform.position.x), 1));
    }

    void OnTriggerEnter()
    {
        StartCoroutine(Utilities.Common.Shake(transform, Vector3.up * 0.2f, 0.1f, 1, false));
        switch (m_ENname)
        {
            case "GameStart":
                if (UserData.UserGamesCount > 20 || (UserData.UserTopWave.Count > 0 ? UserData.UserTopWave.Max() >= 10 : false))//临时数据：10
                {
                    LoadManager.GameDiffSelect();
                }
                else
                {
                    GameManager.RestartInit(0, 0);
                    LoadManager.GameOncall();
                }
                break;
            case "Luck":
                Utilities.UserData.ChangeUserHitLuckTotalTimes(UserData.UserHitLuckTotalTimes + 1);
                var x = Instantiate(m_TextPlayObj, Vector3.up * 0.7f + transform.position, Quaternion.Euler(0, 0, 0), GameObject.Find("MainHall(Clone)").transform);
                var num = Mathf.Min(Utilities.UserData.UserHitLuckTotalTimes / 10 + 1, 9999);
                x.transform.Find("Text").GetComponent<TextMesh>().text = "+" + num.ToString();
                x.GetComponent<MainHallTextPlay>().m_otherTrans = transform;
                break;
            case "Prepare":
                LoadManager.GamePrepare();
                break;
            case "ReturnMainHall":
                LoadManager.GameMainHall();
                break;
            case "Rank":
                LoadManager.GameRank();
                break;
            case "Development":
                LoadManager.GameDevelopment();
                break;
            case "Info":
                LoadManager.GameInfo();
                break;
            default:
                UIManager.OnCommonToast("功能还没做诶");
                break;
        }
    }

}
