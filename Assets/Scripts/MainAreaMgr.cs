using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class MainAreaMgr : MonoBehaviour
{
    public GameObject m_MainHallObj;

    [Header("===底下是prefab===")]
    public GameObject PreparePrefab;
    public GameObject RankPrefab;
    public GameObject MainHallPrefab;
    public GameObject DevPrefab;
    public GameObject DiffSelectPrefab;

    GameObject curRank;
    GameObject curPrepare;
    GameObject curMainHall;
    GameObject curDev;
    GameObject curDiffSelect;
    // Start is called before the first frame update

    // void Awake()
    // {
    //     PreparePrefab = Resources.Load<GameObject>("Prefabs/MainArea/Prepare");
    // }

    void OnEnable()
    {
        LoadManager.m_onGameMainHall += OnGameMainHall;
        LoadManager.m_onGameOncall += OnGameOncall;
        LoadManager.m_onGamePrepare += OnGamePrepare;
        LoadManager.m_onGameRank += OnGameRank;
        LoadManager.m_onGameDevelopment += OnGameDev;
        LoadManager.m_onGameDiffSelect += OnGameDiffSelect;
        LoadManager.m_onGameInfo += OnGameInfo;
    }

    // Update is called once per frame
    void OnDisable()
    {
        LoadManager.m_onGameMainHall -= OnGameMainHall;
        LoadManager.m_onGameOncall -= OnGameOncall;
        LoadManager.m_onGamePrepare -= OnGamePrepare;
        LoadManager.m_onGameRank -= OnGameRank;
        LoadManager.m_onGameDevelopment -= OnGameDev;
        LoadManager.m_onGameDiffSelect -= OnGameDiffSelect;
        LoadManager.m_onGameInfo -= OnGameInfo;

    }


    /// <summary>
    /// 删除所有MainArea
    /// </summary>
    void DestroyAllMainArea()
    {

        var UIlist = GameObject.FindGameObjectsWithTag("SystemMain");
        foreach (var x in UIlist)
        {
            Destroy(x.gameObject);
        }
    }

    void OnGamePrepare()
    {
        if (curPrepare == null)
        {
            DestroyAllMainArea();
            curPrepare = Instantiate(PreparePrefab, new Vector3(-20, -20, 1), Quaternion.Euler(0, 0, 0), transform);
        }
    }
    void OnGameRank()
    {
        if (curRank == null)
        {
            DestroyAllMainArea();
            curRank = Instantiate(RankPrefab, new Vector3(-20, 20, 1), Quaternion.Euler(0, 0, 0), transform);
        }

    }

    void OnGameMainHall()
    {
        if (curMainHall == null)
        {
            DestroyAllMainArea();
            curMainHall = Instantiate(MainHallPrefab, new Vector3(0, 0, 1), Quaternion.Euler(0, 0, 0), transform);
        }


        // Destroy(curPrepare?.gameObject);
        // Destroy(curRank?.gameObject);
        // Destroy(curDiffSelect?.gameObject);
        // // var prepareMainArea = GameObject.FindWithTag("PrepareMainArea")?.gameObject;
        // if (prepareMainArea != null) Destroy(prepareMainArea);
    }

    void OnGameOncall()
    {
        DestroyAllMainArea();
        // var prepareMainArea = GameObject.FindWithTag("PrepareMainArea")?.gameObject;
        // if (prepareMainArea != null) Destroy(prepareMainArea);
    }
    void OnGameDev()
    {
        if (curDev == null)
        {
            DestroyAllMainArea();
            curDev = Instantiate(DevPrefab, new Vector3(0, 0, 1), Quaternion.Euler(0, 0, 0), transform);
        }
        // var prepareMainArea = GameObject.FindWithTag("PrepareMainArea")?.gameObject;
        // if (prepareMainArea != null) Destroy(prepareMainArea);
    }
    void OnGameDiffSelect()
    {
        if (curDiffSelect == null)
        {
            DestroyAllMainArea();
            curDiffSelect = Instantiate(DiffSelectPrefab, new Vector3(0, 0, -48), Quaternion.Euler(0, 0, 0), transform);
        }
        // var prepareMainArea = GameObject.FindWithTag("PrepareMainArea")?.gameObject;
        // if (prepareMainArea != null) Destroy(prepareMainArea);
    }
    void OnGameInfo()
    {
            DestroyAllMainArea();
        // var prepareMainArea = GameObject.FindWithTag("PrepareMainArea")?.gameObject;
        // if (prepareMainArea != null) Destroy(prepareMainArea);
    }

}
