using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubesMgr : MonoBehaviour
{
    //思路是这样的：
    //你的工作是根据需要生成Cubes
    public GameObject Cubes;

    void Start()
    {

        StartCoroutine(LetsGenerateCubes());
    }


    /// <summary>
    /// 发出指令以生成小方块组
    /// </summary>
    /// <returns></returns>
    IEnumerator LetsGenerateCubes()
    {
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }

        float lerpMulti = 0;
        //lerpMulti初始值是0.8，最终是-0.5(弃用)
        //lerpMulti初始值是0，最终是4
        //return update后会回来执行while内部的语句
        // Debug.Log("i:" + i);
        while (lerpMulti < 7)
        {
            // Debug.Log("i:" + i);
            GameObject newCube = Instantiate(Cubes, transform);
            lerpMulti += 0.5f;
            newCube.transform.Find("Square").GetComponent<CubeMgr>().m_range += lerpMulti;
            yield return new WaitForSeconds(0.04f);
            yield return null;
        }
    }

}
