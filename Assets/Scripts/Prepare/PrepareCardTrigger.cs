using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

public class PrepareCardTrigger : MonoBehaviour
{
    public ADAwards aDAwards;
    public Vector3 m_targetLocalPos;
    bool isBannning;

    void Start()
    {
        RefreshAllUI();
        transform.localPosition = Vector3.zero;
        StartCoroutine(StartRotate());
    }

    /// <summary>
    /// 刷新所有UI状态
    /// </summary>
    void RefreshAllUI()
    {
        switch (aDAwards.aDAWARD)
        {
            case ADAwards.ADAWARD.DEATHPROTECT:
                isBannning = Utilities.UserData.UserCurrDeathProtect == aDAwards.InventoryLimit ? true : false;
                transform.Find("Ban").gameObject.SetActive(isBannning ? true : false);
                transform.Find("Front/Canvas/Tags/Get/Text").GetComponent<Text>().text = string.Format("持有:{0}/{1}", UserData.UserCurrDeathProtect, aDAwards.InventoryLimit);
                break;
        }
    }

    void OnTriggerEnter()
    {
        switch (aDAwards.aDAWARD)
        {
            case ADAwards.ADAWARD.DEATHPROTECT:
                if (isBannning)
                {
                    StartCoroutine(Utilities.Common.Shake(transform, Vector3.up * 0.1f, 0, 2));
                    var a = BanTextAppear();
                    StopCoroutine(a);
                    StartCoroutine(a);
                    UIManager.OnCommonToast("已达上限");
                }
                else
                {
                    Utilities.UserData.ChangeUserCurrDeathProtect(UserData.UserCurrDeathProtect + 1);
                    UIManager.OnCommonToast("获得死亡保护，当前:" + Utilities.UserData.UserCurrDeathProtect);
                }
                break;
            case ADAwards.ADAWARD.DOUBLESCORE:
                UIManager.OnCommonToast("获得双倍积分，当前:还没做呢");
                break;
            default:
                UIManager.OnCommonToast("错误，请排查");
                Debug.LogError("错误，请排查");
                break;
        }
        RefreshAllUI();
    }

    IEnumerator BanTextAppear()
    {
        var a = 1f;
        var x = transform.Find("Ban/BanText").GetComponent<TextMesh>();
        while (a > 0.01f)
        {
            a = Mathf.Lerp(a, 0, 0.02f);
            x.color = new Color(x.color.r, x.color.g, x.color.b, a);
            yield return null;
        }
        a = 0;
        x.color = new Color(x.color.r, x.color.g, x.color.b, a);
    }

    IEnumerator StartRotate()
    {
        while (m_targetLocalPos.magnitude - transform.localPosition.magnitude > 0.01f)
        {
            transform.localPosition = Vector3.Slerp(transform.localPosition, m_targetLocalPos, 0.04f);
            yield return null;
        }
        transform.localPosition = m_targetLocalPos;
    }


}
