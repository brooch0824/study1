using UnityEngine;

public class PrepareCard : MonoBehaviour
{
    void OnEnable()
    {
        IUserInput.m_OnMouseDrag += OnMouseDrag;
        transform.eulerAngles = new Vector3(-20, 0, 0);
        StartCoroutine(Utilities.Common.SmoothRotate(transform, 0, 720));

        //transform.Rotate(Vector3.up, y);
    }

    void OnDisable()
    {

        IUserInput.m_OnMouseDrag -= OnMouseDrag;
    }

    void OnMouseDrag()
    {
        if (IUserInput.mousedrag == IUserInput.MOUSEDRAG.RIGHT)
            StartCoroutine(Utilities.Common.SmoothRotate(transform, 0, -120));
        else if (IUserInput.mousedrag == IUserInput.MOUSEDRAG.LEFT)
            StartCoroutine(Utilities.Common.SmoothRotate(transform, 0, 120));
    }




    //Mathf.Abs(total - 180) > 0.1f
}
