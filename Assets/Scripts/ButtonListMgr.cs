using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonListMgr : MonoBehaviour
{
    public GameObject m_hallCubeObj;
    public Transform m_hallCubePath;
    public GameSystems m_gameSystems;

    string nowSee;
    int buttonListCount;
    public List<GameObject> buttonObjList = new List<GameObject>();




    void OnEnable()
    {
        //LoadManager.m_onGameMainHall += OnGameMainHall;
        IUserInput.m_OnMouseDrag += ButtonListOnMouseDrag;
    }

    void OnDisable()
    {

        //LoadManager.m_onGameMainHall -= OnGameMainHall;
        IUserInput.m_OnMouseDrag -= ButtonListOnMouseDrag;

    }

    void Start()
    {
        buttonListCount = 5;

        nowSee = m_gameSystems.m_ENname[buttonListCount / 2];
        GenerateHallCube();
    }


    void OnGameMainHall()
    {
        GenerateHallCube();
    }


    void ButtonListOnMouseDrag()
    {
        //Debug.Log("success now : " + mousedrag);
        if (GameManager.gAMESTATE == GameManager.GAMESTATE.MAINHALL)
        {
            if (IUserInput.mousedrag != IUserInput.MOUSEDRAG.NULL)
            {
                switch (IUserInput.mousedrag)
                {
                    case IUserInput.MOUSEDRAG.RIGHT:
                        //nowSee = m_gameSystems.m_ENname[m_gameSystems.m_ENname.IndexOf(nowSee) + 1];
                        //CheckNowSee(1);
                        DestroyTargetInButtonObjList("right");
                        StartCoroutine(RollButtonList(Vector3.right * 2));
                        break;
                    case IUserInput.MOUSEDRAG.LEFT:
                        //CheckNowSee(-1);
                        DestroyTargetInButtonObjList("left");
                        StartCoroutine(RollButtonList(Vector3.left * 2));
                        break;
                }
                //ResetMouseDrag();//不加这个有可能一帧执行多次
            }
        }
    }


    int CheckNowSee(int Change)
    {
        var idx = m_gameSystems.m_ENname.IndexOf(nowSee);
        int leftone = idx - 1;
        int rightone = idx + 1;
        if (leftone < 0) leftone += m_gameSystems.m_ENname.Count;
        if (rightone > m_gameSystems.m_ENname.Count - 1) rightone -= m_gameSystems.m_ENname.Count;

        switch (Change)
        {
            case 1:
                idx = rightone;
                break;
            case -1:
                idx = leftone;
                break;
            case 0:
                break;
            default:
                Debug.LogError("现在只支持+-1哦");
                break;
        }
        nowSee = m_gameSystems.m_ENname[idx];
        return idx;
    }

    IEnumerator RollButtonList(Vector3 targetDeltaPos)
    {
        //可能有问题，就是卡了一下以后玩家顶不到按钮
        IUserInput.canInteract = false;
        Debug.Log("RollButtonList");
        var targetPos = transform.position + targetDeltaPos;
        var i = 0;//如果50次之内没有达到目标就自动到达目标
        while (Mathf.Abs(targetPos.magnitude - transform.position.magnitude) > 0.05f && i<50)
        {
            transform.position = Vector3.Slerp(transform.position, targetPos, 0.2f);
            i++;
            yield return null;
        }
        transform.position = targetPos;
        //Debug.Log(targetPos);
        IUserInput.canInteract = true;
    }

    void GenerateHallCube()
    {
        Debug.Log("GenerateHallCube");
        Vector3 _leftestVector = new Vector3(-4, 1.5f, 1);

        buttonObjList.Clear();
        foreach (Transform child in m_hallCubePath)
        {
            Destroy(child.gameObject);
        }

        for (int j = 0; j < buttonListCount; j++)
        {
            GameObject x = Instantiate(m_hallCubeObj, _leftestVector + 2 * j * Vector3.right, Quaternion.Euler(0, 0, 0), m_hallCubePath);

            x.GetComponent<MainHallCubeTrigger>().m_ENname = m_gameSystems.m_ENname[j];
            x.GetComponent<MainHallCubeTrigger>().m_CNname = m_gameSystems.m_CNname[j];
            buttonObjList.Add(x);
            // x.transform.Find("Num").GetComponent<TextMesh>().text = m_gameSystems.m_CNname[j];
        }

    }

    void DestroyTargetInButtonObjList(string whichToDestroy)
    {
        Vector3 newPos;
        int nowSeeIdx;
        switch (whichToDestroy)
        {
            case "left":
                newPos = buttonObjList[buttonObjList.Count - 1].transform.position + Vector3.right * 2;
                GameObject x = Instantiate(m_hallCubeObj, newPos, Quaternion.Euler(0, 0, 0), m_hallCubePath);
                nowSeeIdx = CheckNowSee(1);
                x.GetComponent<MainHallCubeTrigger>().m_ENname = m_gameSystems.m_ENname[nowSeeIdx + 2 > m_gameSystems.m_ENname.Count - 1 ? nowSeeIdx + 2 - m_gameSystems.m_ENname.Capacity : nowSeeIdx + 2];
                x.GetComponent<MainHallCubeTrigger>().m_CNname = m_gameSystems.m_CNname[nowSeeIdx + 2 > m_gameSystems.m_ENname.Count - 1 ? nowSeeIdx + 2 - m_gameSystems.m_ENname.Capacity : nowSeeIdx + 2];
                buttonObjList.Add(x);
                Destroy(buttonObjList[0].gameObject);
                buttonObjList.RemoveAt(0);
                break;
            case "right":
                newPos = buttonObjList[0].transform.position + Vector3.left * 2;
                GameObject y = Instantiate(m_hallCubeObj, newPos, Quaternion.Euler(0, 0, 0), m_hallCubePath);
                nowSeeIdx = CheckNowSee(-1);
                y.GetComponent<MainHallCubeTrigger>().m_ENname = m_gameSystems.m_ENname[nowSeeIdx - 2 < 0 ? nowSeeIdx - 2 + m_gameSystems.m_ENname.Count : nowSeeIdx - 2];
                y.GetComponent<MainHallCubeTrigger>().m_CNname = m_gameSystems.m_CNname[nowSeeIdx - 2 < 0 ? nowSeeIdx - 2 + m_gameSystems.m_ENname.Count : nowSeeIdx - 2];
                buttonObjList.Insert(0, y);
                Destroy(buttonObjList[buttonObjList.Count - 1].gameObject);
                buttonObjList.RemoveAt(buttonObjList.Count - 1);
                break;
            default:
                Debug.LogError("到底要销毁哪个");
                break;
        }


    }
}
