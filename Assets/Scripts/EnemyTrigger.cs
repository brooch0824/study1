using UnityEngine;

public class EnemyTrigger : MonoBehaviour
{
    public bool isTriggerThis;
    public static bool isTriggerAny;
    void OnTriggerEnter(Collider other)
    {
        Debug.Log("other:" + other.name);

        //SendMessageUpwards("OnEnemyTriggerEnter");
        if (other.tag.Equals("Player"))
        {
            isTriggerThis = true;
            isTriggerAny = true;
            transform.parent.GetComponent<Enemy>().OnPlayerTrigger();
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            isTriggerThis = false;
            isTriggerAny = false;
        }
    }
}
