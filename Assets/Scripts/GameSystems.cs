using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "工具/系统对照表", fileName = "GameSystems")]
public class GameSystems : ScriptableObject
{

    [SerializeField]public List<string> m_CNname = new List<string>();
    [SerializeField]public List<string> m_ENname = new List<string>();


}
