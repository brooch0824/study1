using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TimeLimit : MonoBehaviour
{
    public Transform TimeText;
    public Transform TimeBar;
    GameManager gameManager;
    float target;

    void Awake()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        TimeText = transform.Find("LimitTime");
        TimeBar = transform.Find("TimeBar");
    }
    void OnEnable()
    {
        //GameManager.m_onGameOver += GameOver;
        GameManager.m_onGameRestartOther += Restart;
    }


    void OnDisable()
    {
        //GameManager.m_onGameOver -= GameOver;
        GameManager.m_onGameRestartOther -= Restart;
    }

    // void Start()
    // {
    //     Restart();
    // }

    public void Restart()
    {
        TimeText.localScale = Vector3.one;
        TimeBar.localScale = Vector3.one;
        TimeText.transform.Find("Particle").gameObject.SetActive(false);
        StartCoroutine(CalTimeText());
        Debug.Log("Restart Timelimit");
    }

    /// <summary>
    /// 计算文本该显示什么
    /// </summary>
    IEnumerator CalTimeText()
    {
        while (GameManager.gAMESTATE == GameManager.GAMESTATE.GAMEONCALL)
        {
            TimeText.GetComponent<Text>().text = "撞飞[小]方块吧";
            ClickToReady();
            yield return null;
        }

        while (GameManager.gAMESTATE == GameManager.GAMESTATE.GAMING)
        {
            //TimeText.GetComponent<Text>().fontSize = 140;

            //游戏结束以前一直检测
            if (EnemyTrigger.isTriggerAny)
            {
                TimeText.GetComponent<Text>().color = Color.white;
                TimeText.GetComponent<Text>().text = "你撞得好呀";
                StartCoroutine(Utilities.Common.Shake(TimeText.GetComponent<RectTransform>(), Vector3.up * 10, 0.03f, 3));
                TimeText.transform.Find("Particle").gameObject.SetActive(false);
                TimeText.transform.Find("Particle").gameObject.SetActive(true);
                yield return new WaitForSeconds(0.5f);
            }

            TimeText.GetComponent<Text>().text = Mathf.Max(GameManager.currentTime, 0).ToString("f2");


            if (GameManager.currentTime < 1)
            {
                TimeText.GetComponent<Text>().color = Color.red;
            }
            else
            {
                TimeText.GetComponent<Text>().color = Color.white;
            }

            if (GameManager.currentTime > -0.3f)//缓冲时间
            {
                if (IUserInput.mousedrag == IUserInput.MOUSEDRAG.NULL)
                    GameManager.currentTime -= Time.deltaTime;
            }
            else
            {
                gameManager.CheckIfGameOver();
                GameManager.gAMEOVERTYPE = GameManager.GAMEOVERTYPE.TIMELIMIT;
            }

            float barMulti = Mathf.Max(0, GameManager.currentTime / GameManager.limitTime);
            TimeBar.localScale = new Vector3(barMulti, 1, 1);

            yield return null;
        }


        if (GameManager.gAMEOVERTYPE == GameManager.GAMEOVERTYPE.WRONGHIT)
        {
            TimeText.GetComponent<Text>().text = "撞错啦T^T";
            StartCoroutine(Utilities.Common.Shake(TimeText.GetComponent<RectTransform>(), Vector3.left * 20, 0.03f, 5));
        }
        if (GameManager.gAMEOVERTYPE == GameManager.GAMEOVERTYPE.TIMELIMIT)
        {
            TimeText.GetComponent<Text>().text = GameManager.currentTime.ToString("时间到");
        }




        yield return new WaitForSecondsRealtime(1f);
        while (GameManager.gAMESTATE == GameManager.GAMESTATE.GAMEOVER && TimeText.localScale.magnitude > 0.01f)
        {
            TimeText.localScale = Vector3.Slerp(TimeText.localScale, new Vector3(0, 0, 1), 0.1f);
            TimeBar.localScale = Vector3.Slerp(TimeBar.localScale, new Vector3(0, 0, 1), 0.1f);
            yield return null;
        }
    }


    /// <summary>
    /// 游戏结束时触发的事情
    /// </summary>
    /// <returns></returns>
    IEnumerator GameOver()
    {
        Debug.Log("Success TimeLimit GameOver");
        yield return new WaitForSecondsRealtime(1f);
        while (GameManager.gAMESTATE == GameManager.GAMESTATE.GAMEOVER && TimeText.localScale.magnitude > 0.01f)
        {
            TimeText.localScale = Vector3.Slerp(TimeText.localScale, new Vector3(0, 0, 1), 0.1f);
            TimeBar.localScale = Vector3.Slerp(TimeBar.localScale, new Vector3(0, 0, 1), 0.1f);
            yield return null;
        }

    }

    void ClickToReady()
    {
        if (TimeText.GetComponent<Text>().color.a >= 0.95f)
        {
            target = 0;
        }
        else if (TimeText.GetComponent<Text>().color.a <= 0.05f)
        {
            target = 1;
        }
        TimeText.GetComponent<Text>().color = new Color(1, 1, 1, Mathf.Lerp(TimeText.GetComponent<Text>().color.a, target, 0.01f));
    }
}

