using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class RankMgr : MonoBehaviour
{
    public Transform m_unitPath;
    [Header("=====以下是Prefab=====")]
    public GameObject m_unitPrefab;

    void OnEnable()
    {
        IUserInput.m_OnMouseDrag += OnMouseDrag;
    }
    void OnDisable()
    {
        IUserInput.m_OnMouseDrag -= OnMouseDrag;
    }

    void Start()
    {
        // float f = (float)Screen.width / Screen.height;
        // transform.Find("RankCard").localScale *= f * 16f / 9f;


        foreach (Transform child in m_unitPath)
        {
            Destroy(child?.gameObject);
        }


        StartCoroutine(CreateUnit());

    }

    void OnMouseDrag()
    {
        if (IUserInput.mousedrag == IUserInput.MOUSEDRAG.RIGHT)
            StartCoroutine(Utilities.Common.SmoothRotate(transform.Find("RankCard/LocalRank"), 0, -360));
        else if (IUserInput.mousedrag == IUserInput.MOUSEDRAG.LEFT)
            StartCoroutine(Utilities.Common.SmoothRotate(transform.Find("RankCard/LocalRank"), 0, 360));
    }

    IEnumerator CreateUnit()
    {
        for (int i = 0; i < 10; i++)
        {
            var x = Instantiate(m_unitPrefab, m_unitPath);
            x.transform.Find("Rank").GetComponent<Text>().text = (i + 1).ToString();
            if (i < Utilities.UserData.UserTopPair.Count)
            {
                x.transform.Find("RankWaves").GetComponent<Text>().text = "第 " + Utilities.UserData.UserTopPair[i].Wave.ToString() + " 波";
                x.transform.Find("RankScore").GetComponent<Text>().text = Utilities.UserData.UserTopPair[i].Score.ToString() + " 分";
                var colorRankScore = x.transform.Find("RankScore").GetComponent<Text>().color;
                x.transform.Find("RankScore").GetComponent<Text>().color = new Color(colorRankScore.r, colorRankScore.g, colorRankScore.b, 1);
                var colorRankWaves = x.transform.Find("RankWaves").GetComponent<Text>().color;
                x.transform.Find("RankWaves").GetComponent<Text>().color = new Color(colorRankWaves.r, colorRankWaves.g, colorRankWaves.b, 1);
            }
            yield return new WaitForSeconds(0.1f);
            yield return null;
        }

    }

}
