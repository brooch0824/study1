using UnityEngine;

public class DisableIUserInput : MonoBehaviour
{
    // Start is called before the first frame update
    void OnMouseOver()
    {
        IUserInput.canInteract = false;
    }

    void OnMouseUp()
    {
        //IUserInput.canInteract = true;
    }

    void Update()
    {
        // if(Camera.main.ScreenPointToRay(Input.mousePosition))
        Debug.Log(IUserInput.canInteract);
    }
}
