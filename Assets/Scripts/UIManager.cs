using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private static GameObject CommonToast;
    public GameObject DragTips;
    GameObject _curDragTips;
    Animator animator;
    [Header("=====以下为prefab=====")]
    public GameObject m_PrepareUIPrefab;
    public GameObject m_RankUIPrefab;
    public GameObject m_MainHallUIPrefab;
    public GameObject m_DevUIPrefab;
    public GameObject m_DiffSelectUIPrefab;
    GameObject _curPrepareUI;
    GameObject _curRankUI;
    GameObject _curMainHallUI;
    GameObject _curDevUI;
    GameObject _curDiffSelectUI;


    void Awake()
    {
        animator = GetComponent<Animator>();
        CommonToast = Resources.Load<GameObject>("Prefabs/Common/Toast");
    }

    public static void OnCommonToast(string str)
    {
        var a = Instantiate(CommonToast, GameObject.Find("MainArea/UI/Canvas").transform);
        a.GetComponent<CommonToast>().texture.text = str;

    }

    void OnEnable()
    {
        IUserInput.m_OnMouseDrag += TryDestroyDragTips;
        LoadManager.m_onGameMainHall += OnGameMainHall;
        LoadManager.m_onGameOncall += OnGameOncall;
        LoadManager.m_onGamePrepare += OnGamePrepare;
        LoadManager.m_onGameRank += OnGameRank;
        LoadManager.m_onGameDevelopment += OnGameDev;
        LoadManager.m_onGameDiffSelect += OnGameDiffSelect;
        LoadManager.m_onGameInfo += OnGameInfo;
    }
    void OnDisable()
    {
        IUserInput.m_OnMouseDrag -= TryDestroyDragTips;
        LoadManager.m_onGameMainHall -= OnGameMainHall;
        LoadManager.m_onGameOncall -= OnGameOncall;
        LoadManager.m_onGamePrepare -= OnGamePrepare;
        LoadManager.m_onGameRank -= OnGameRank;
        LoadManager.m_onGameDevelopment -= OnGameDev;
        LoadManager.m_onGameDiffSelect -= OnGameDiffSelect;
        LoadManager.m_onGameInfo -= OnGameInfo;
    }

    void TryDestroyDragTips()
    {
        Destroy(_curDragTips?.gameObject);
    }


    /// <summary>
    /// 删除所有UI
    /// </summary>
    void DestroyAllUI()
    {

        var UIlist = GameObject.FindGameObjectsWithTag("SystemMainUI");
        foreach (var x in UIlist)
        {
            Destroy(x.gameObject);
        }
    }

    void OnGameMainHall()
    {
        if (_curMainHallUI == null)
        {
            DestroyAllUI();
            _curMainHallUI = Instantiate(m_MainHallUIPrefab, transform.Find("Canvas"));
            _curDragTips = Instantiate(DragTips, GameObject.Find("MainArea/Player").transform);
        }
        // Destroy(_curPrepareUI?.gameObject);
        // Destroy(_curRankUI?.gameObject);
        // Destroy(_curDevUI?.gameObject);
        // Destroy(_curDiffSelectUI?.gameObject);


    }
    void OnGameOncall()
    {
        DestroyAllUI();

    }

    void OnGamePrepare()
    {
        if (_curPrepareUI == null)
        {
            DestroyAllUI();
            _curPrepareUI = Instantiate(m_PrepareUIPrefab, transform.Find("Canvas"));
        }
        //Destroy(_curMainHallUI?.gameObject);

    }

    void OnGameRank()
    {
        if (_curRankUI == null)
        {
            DestroyAllUI();
            _curRankUI = Instantiate(m_RankUIPrefab, transform.Find("Canvas"));
        }
        //Destroy(_curMainHallUI?.gameObject);

    }

    void OnGameDev()
    {
        if (_curDevUI == null)
        {
            DestroyAllUI();
            _curDevUI = Instantiate(m_DevUIPrefab, transform.Find("Canvas"));
        }
        //Destroy(_curMainHallUI?.gameObject);

    }
    void OnGameDiffSelect()
    {
        if (_curDiffSelectUI == null)
        {
            DestroyAllUI();
            _curDiffSelectUI = Instantiate(m_DiffSelectUIPrefab, transform.Find("Canvas"));
        }
        //Destroy(_curMainHallUI?.gameObject);

    }
    void OnGameInfo()
    {
            DestroyAllUI();
        //Destroy(_curMainHallUI?.gameObject);

    }



    // IEnumerator StartDragTips()
    // {
    //     _curDragTips = Instantiate(DragTips, GameObject.Find("MainArea/Player").transform);
    //     yield return new WaitForSeconds(3);
    //     while (!Input.GetMouseButtonUp(0))
    //     {
    //         yield return null;
    //     }
    //     animator.Play("Default");
    //     Destroy(_curDragTips?.gameObject);
    // }

    /// <summary>
    /// 用,分割
    /// </summary>
    /// <param name="pathandstr"></param>
    void ChangeText(string pathandstr)
    {
        var splits = pathandstr.Split(',');
        var path = splits[0];
        var str = splits[1];
        GameObject.Find(path).GetComponent<Text>().text = str;

        if (splits[1].Equals("GameManager.currentDiffiLv"))
            GameObject.Find(path).GetComponent<Text>().text = GameManager.currentDiffiLv.ToString();

    }
}
