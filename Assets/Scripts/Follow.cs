using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour
{
    //public GameObject target;
    public Vector3 targetPos;
    public Vector3 targetRot;
    public List<Vector3> m_CameraPos = new List<Vector3>();
    public List<Vector3> m_CameraRot = new List<Vector3>();

    float multi;//插值速率
    float waitfor;//等待时间
                  // void Update()
                  // {
                  //     if (GameManager.gAMESTATE == GameManager.GAMESTATE.MAINHALL)
                  //     {
                  //         targetPos = m_CameraPos[0];
                  //         targetRot = m_CameraRot[0];
                  //         StartCoroutine(CameraMove());
                  //     }
                  //     if (GameManager.gAMESTATE == GameManager.GAMESTATE.GAMING)
                  //     {
                  //         targetPos = GameManager.playerObj.transform.position;
                  //         targetPos.z = m_CameraRot[1].z;
                  //         targetRot = m_CameraRot[1];
                  //         StartCoroutine(CameraMove());
                  //     }

    // }

    void Awake()
    {
        transform.position = new Vector3(0, -20, -10);
    }

    void OnEnable()
    {
        LoadManager.m_onGameMainHall += OnMainHall;
        LoadManager.m_onGameOncall += OnGameOncall;
        LoadManager.m_onGaming += OnGaming;
        LoadManager.m_onGameOver += OnGameOver;
        LoadManager.m_onGamePrepare += OnPrepare;
        LoadManager.m_onGameRank += OnRank;
        LoadManager.m_onGameDevelopment += OnDev;
        LoadManager.m_onGameDiffSelect += OnGameDiffSelect;
        LoadManager.m_onGameInfo += OnGameInfo;
    }
    void OnDisable()
    {
        LoadManager.m_onGameMainHall -= OnMainHall;
        LoadManager.m_onGameOncall -= OnGameOncall;
        LoadManager.m_onGaming -= OnGaming;
        LoadManager.m_onGameOver -= OnGameOver;
        LoadManager.m_onGamePrepare -= OnPrepare;
        LoadManager.m_onGameRank -= OnRank;
        LoadManager.m_onGameDevelopment -= OnDev;
        LoadManager.m_onGameDiffSelect -= OnGameDiffSelect;
        LoadManager.m_onGameInfo -= OnGameInfo;
    }

    void OnMainHall()
    {
        var a = CameraMove();
        StopCoroutine(a);
        targetPos = m_CameraPos[0];
        targetRot = m_CameraRot[0];
        waitfor = 0;
        multi = 0.04f;
        StartCoroutine(a);
    }

    void OnGameOncall()
    {
        var a = CameraMove();
        StopCoroutine(a);
        targetPos = m_CameraPos[1];
        targetRot = m_CameraRot[1];
        waitfor = 0;
        multi = 0.06f;
        StartCoroutine(a);

    }

    //这是运行中绑定玩家的方块
    void OnGaming()
    {
        var a = CameraMove();
        StopCoroutine(a);
        //targetPos = GameManager.playerObj.transform.position;
        targetPos.z = m_CameraPos[1].z;
        targetRot = m_CameraRot[1];
        waitfor = 0;
        multi = 0.04f;
        StartCoroutine(a);
    }

    void OnGameOver()
    {
        var a = CameraMove();
        StopCoroutine(a);
        targetPos = m_CameraPos[2];
        targetRot = m_CameraRot[2];
        waitfor = 1f;
        multi = 0.15f;
        StartCoroutine(a);
    }

    void OnPrepare()
    {
        var a = CameraMove();
        StopCoroutine(a);
        targetPos = m_CameraPos[3];
        targetRot = m_CameraRot[3];
        waitfor = 0f;
        multi = 0.15f;
        StartCoroutine(a);
    }
    void OnRank()
    {
        var a = CameraMove();
        StopCoroutine(a);
        targetPos = m_CameraPos[4];
        targetRot = m_CameraRot[4];
        waitfor = 0f;
        multi = 0.08f;
        StartCoroutine(a);
    }
    void OnDev()
    {
        var a = CameraMove();
        StopCoroutine(a);
        targetPos = m_CameraPos[5];
        targetRot = m_CameraRot[5];
        waitfor = 0f;
        multi = 0.04f;
        StartCoroutine(a);
    }
    void OnGameDiffSelect()
    {
        var a = CameraMove();
        StopCoroutine(a);
        targetPos = m_CameraPos[6];
        targetRot = m_CameraRot[6];
        waitfor = 0f;
        multi = 0.04f;
        StartCoroutine(a);
    }
    
    void OnGameInfo()
    {
        var a = CameraMove();
        StopCoroutine(a);
        targetPos = m_CameraPos[7];
        targetRot = m_CameraRot[7];
        waitfor = 0f;
        multi = 0.04f;
        StartCoroutine(a);
    }

    IEnumerator CameraMove()
    {
        // while (targetPos.magnitude - transform.position.magnitude > 0.01f && targetRot.magnitude - transform.rotation.eulerAngles.magnitude > 0.01f)
        IUserInput.canInteract = false;
        yield return new WaitForSecondsRealtime(waitfor);
        while (Mathf.Abs(targetPos.magnitude - transform.localPosition.magnitude) > 0.001f)
        {
            transform.localPosition = Vector3.Slerp(transform.localPosition, targetPos, multi);
            transform.localEulerAngles = Vector3.Slerp(transform.localEulerAngles, targetRot, multi);
            if (Mathf.Abs(targetPos.magnitude - transform.localPosition.magnitude) > 0.5f) IUserInput.canInteract = true;
            yield return new WaitForFixedUpdate();
        }
        transform.localPosition = targetPos;
        transform.localEulerAngles = targetRot;
    }
}
