using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utilities;


public class InfoGroup : MonoBehaviour
{
    public List<Transform> infos = new();
    // Start is called before the first frame update
    void Start()
    {

        infos[0].Find("TextR").GetComponent<Text>().text = UserData.LoadDateCount.ToString();
        infos[1].Find("TextR").GetComponent<Text>().text = UserData.UserGamesCount.ToString();
        infos[2].Find("TextR").GetComponent<Text>().text = UserData.UserTopWave[0].ToString();
        infos[3].Find("TextR").GetComponent<Text>().text = UserData.UserTopScore[0].ToString();
        infos[4].Find("TextR").GetComponent<Text>().text = UserData.HitEnemySuccessCount.ToString();
    }

    // Update is called once per frame
    void Update()
    {
    }
}
