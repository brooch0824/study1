using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

public class GameManager : MonoBehaviour
{
    public enum GAMEOVERTYPE
    {
        NULL,
        TIMELIMIT,
        WRONGHIT
    }

    public enum GAMESTATE
    {
        MAINHALL,//大厅
        GAMEONCALL,//待机
        GAMING,//游戏中
        GAMEOVER,//游戏结束
        PREPARE,//战前准备
        RANK,//排行榜
        DEV,//养成
        DIFFSELECT,//难度选择
        INFO,//在看角色详情
    }
    public delegate void GameFunc();
    public static event GameFunc m_onGameRestartOther;//游戏重启时会发生的事情
    public static GAMESTATE gAMESTATE;//当前游戏状态
    public static GAMEOVERTYPE gAMEOVERTYPE;
    public static int UserNum { get; private set; }

    public static int currentWave;
    public static float limitTime;
    public static float currentTime;
    public static float currentDiffiLv;
    public static int currentTotalScore;
    static int initWave;
    static int initScore;
    public List<int> numList = new List<int>();
    public List<Vector3> enemyInitPosList = new List<Vector3>();//敌人的初始位置坐标
    public List<GameObject> enemyList = new List<GameObject>();//敌人汇总
    GameObject enemysPathObj;
    GameObject playerPathObj;
    GameObject enemyPrefab;
    public GameObject playerPrefab;
    GameObject myUIObj;
    public GameObject playerObj;
    public GameObject m_buttonList;
    GameObject gameOverObj;
    int vecNum;
    //public GameObject gameover;
    int lowerCount;//小于玩家的数量要创建几个
    int enemyCount;//敌人数量
    int currDPRemain;//还剩下几次死亡保护机会
    Vector3 gameStartPos;//游戏开始坐标


    void Awake()
    {
        Utilities.UserSave.LoadData();


        myUIObj = GameObject.Find("MainArea/UI");
        //玩家的盒子是重新生成的
        playerObj = GameObject.FindWithTag("Player");
        gameOverObj = GameObject.Find("MainArea/UI/Canvas/GameOver");


        enemyPrefab = Resources.Load<GameObject>("Prefabs/Enemy");
        //playerPrefab = Resources.Load<GameObject>("Prefabs/Player");
        enemysPathObj = GameObject.Find("MainArea/CompareArea/Enemys");
        playerPathObj = GameObject.Find("MainArea/Player");

        Utilities.UserData.ResourcesLoad();

        RegistUserNum();

        Application.targetFrameRate = 160;
    }

    void OnEnable()
    {
        LoadManager.m_onGameOncall += OnGameOncall;
        LoadManager.m_onGaming += OnGaming;
        LoadManager.m_onGameMainHall += OnGameMainHall;
        LoadManager.m_onGameOver += OnGameOver;
        LoadManager.m_onGamePrepare += OnGamePrepare;
        LoadManager.m_onGameRank += OnGameRank;
        LoadManager.m_onGameDevelopment += OnGameDev;
        LoadManager.m_onGameDiffSelect += OnGameDiffSelect;
        LoadManager.m_onGameInfo += OnGameInfo;
        // m_onGameRestart += Restart;
    }

    void OnDisable()
    {
        LoadManager.m_onGameOncall -= OnGameOncall;
        LoadManager.m_onGaming -= OnGaming;
        LoadManager.m_onGameMainHall -= OnGameMainHall;
        LoadManager.m_onGameOver -= OnGameOver;
        LoadManager.m_onGamePrepare -= OnGamePrepare;
        LoadManager.m_onGameRank -= OnGameRank;
        LoadManager.m_onGameDevelopment -= OnGameDev;
        LoadManager.m_onGameInfo -= OnGameInfo;
        // m_onGameRestart -= Restart;
    }

    /// <summary>
    /// 重新开始游戏的初始状态设置
    /// </summary>
    public static void RestartInit(int _initWave, int _initScore)
    {
        initWave = _initWave;
        initScore = _initScore;
    }

    /// <summary>
    /// 进入待机阶段预备的东西
    /// </summary>
    void OnGameOncall()
    {
        IUserInput.canInteract = true;
        Time.timeScale = 1;
        //StopAllCoroutines();


        gAMEOVERTYPE = GAMEOVERTYPE.NULL;

        myUIObj.transform.Find("Canvas/CompareArea").gameObject.SetActive(true);
        myUIObj.transform.Find("Canvas/GameOver").gameObject.SetActive(false);
        Restart();
    }


    void OnGaming()
    {
        IUserInput.canInteract = true;
    }

    // bool IsSameDay(System.DateTime d1, System.DateTime d2)
    // {
    //     Debug.Log($"d1 {d1.Date}");
    //     Debug.Log($"d2 {d2.Date}");
    //     Debug.Log(d2.Date == d1.Date);

    //     return d1.Date == d2.Date;
    // }

    /// <summary>
    /// 进入大厅时预备的东西
    /// </summary>
    void OnGameMainHall()
    {
        // System.DateTime now = System.DateTime.Now;
        // if (!IsSameDay(now, UserData.LastLoadDate))
        // {
        //     UserData.ChangeLoadDateCount(UserData.LoadDateCount + 1);
        //     UserData.ChangeLastLoadDate(now);
        // }
        playerPathObj.transform.position = Vector3.forward;
        IUserInput.canInteract = true;
        Time.timeScale = 1;
        StopAllCoroutines();


        RegistUserNum();
        IUserInput.mouseDownPos = Input.mousePosition;
        IUserInput.mousedrag = IUserInput.MOUSEDRAG.NULL;

        myUIObj.transform.Find("Canvas/CompareArea").gameObject.SetActive(false);
        myUIObj.transform.Find("Canvas/GameOver").gameObject.SetActive(false);
        ClearEnemy();
        RegeneratePlayer(Vector3.forward);

        //myUIObj.transform.Find("Canvas/CompareArea").gameObject.SetActive(false); //这个是进入主界面的时候发生
    }
    /// <summary>
    /// 游戏结束
    /// </summary>
    public void CheckIfGameOver()
    {
        if (currDPRemain > 0)
        {
            OnDP();
            return;
        }

        // StartCoroutine(GameOverCo());
        LoadManager.GameOver();
    }

    /// <summary>
    /// 触发死亡保护
    /// </summary>
    void OnDP()
    {
        currDPRemain -= 1;
        Utilities.UserData.ChangeUserCurrDeathProtect(UserData.UserCurrDeathProtect - 1);
    }


    void OnGameOver()
    {
        IUserInput.canInteract = false;
        GameResult gameResult = new GameResult(UserNum, currentWave);
        Utilities.UserData.TryChangeTopScoreWavePair(gameResult);
        Utilities.UserData.ChangeUserGamesCount(UserData.UserGamesCount + 1);
        UserSave.SaveData();

        playerObj.GetComponent<Animator>().ResetTrigger("DragUp");
        playerObj.GetComponent<Animator>().ResetTrigger("DragDown");
        playerObj.GetComponent<Animator>().ResetTrigger("DragLeft");
        playerObj.GetComponent<Animator>().ResetTrigger("DragRight");
        StopAllCoroutines();
        StartCoroutine(GameOverCo());

    }


    IEnumerator GameOverCo()
    {
        yield return new WaitForSeconds(1);
        ClearEnemy();

        playerObj.GetComponent<Animator>().Play("Player_GameOver");
        playerObj.GetComponent<Animator>().SetLayerWeight(1, 0);//Eyes权重变为1
        playerObj.GetComponent<Animator>().SetLayerWeight(0, 1);//BaseLayer权重变为0(变不了，先放着不管)

        // float a = MainCamObj.transform.position.z;
        // while (a < -4.1f)
        // {
        //     a = Mathf.Lerp(a, -4, 0.1f);
        //     MainCamObj.transform.position = new Vector3(0, 0, a);
        //     yield return null;
        // }
        yield return new WaitForSeconds(2);
        gameOverObj.SetActive(true);
        Time.timeScale = 0;

    }


    /// <summary>
    /// 玩家战前准备时
    /// </summary>
    void OnGamePrepare()
    {
        //把玩家移过去（-5，-5,0）
        //如果因为未知原因玩家不存在就创造一个(算了还是报错吧)
        IUserInput.canInteract = true;
        Time.timeScale = 1;
        if (playerObj == null)
        {
            Debug.LogError("玩家怎么会不存在呢");
        }
        else
        {
            playerObj.transform.Find("Body/Trails/Trail").gameObject.SetActive(false);
            playerPathObj.transform.localPosition = new Vector3(-20, -24f, -6);
            RegeneratePlayer(new Vector3(-20, -24f, -5));
            //playerObj.transform.Find("Body/Trails/Trail").gameObject.SetActive(true);
        }

        // myUIObj.transform.Find("Canvas/MainHall").gameObject.SetActive(false);
        myUIObj.transform.Find("Canvas/CompareArea").gameObject.SetActive(false);
        myUIObj.transform.Find("Canvas/GameOver").gameObject.SetActive(false);
        // myUIObj.transform.Find("Canvas/Prepare").gameObject.SetActive(true);
    }

    /// <summary>
    /// 玩家在看排行榜的时候
    /// </summary>
    void OnGameRank()
    {
        IUserInput.canInteract = true;
        Time.timeScale = 1;

        playerPathObj.transform.localPosition = new Vector3(-20, 16, -6);
        RegeneratePlayer(new Vector3(-20, 16, -5));
        //playerObj.transform.Find("Body/Trails/Trail").gameObject.SetActive(true);
    }

    /// <summary>
    /// 玩家在看养成的时候
    /// </summary>
    void OnGameDev()
    {
        IUserInput.canInteract = true;
        Time.timeScale = 1;

        playerPathObj.transform.localPosition = new Vector3(0, -3.5f, 21);
        RegeneratePlayer(playerPathObj.transform.localPosition + Vector3.forward);
        //playerObj.transform.Find("Body/Trails/Trail").gameObject.SetActive(true);
    }


    /// <summary>
    /// 玩家在看难度选择的时候
    /// </summary>
    void OnGameDiffSelect()
    {
        IUserInput.canInteract = true;
        Time.timeScale = 1;

        playerPathObj.transform.localPosition = new Vector3(0, -3.5f, -50);
        RegeneratePlayer(playerPathObj.transform.localPosition + Vector3.forward);
        //playerObj.transform.Find("Body/Trails/Trail").gameObject.SetActive(true);
    }

    /// <summary>
    /// 玩家在看详情的时候
    /// </summary>
    void OnGameInfo()
    {
        IUserInput.canInteract = true;
        Time.timeScale = 1;

        //playerObj.transform.Find("Body/Trails/Trail").gameObject.SetActive(true);
    }



    /// <summary>
    /// 生成新的一轮
    /// </summary> <summary>
    public void NewWave()
    {
        //=====重要=====
        //暂时的，后面要根据算法去换，先实现
        //后面还会根据难度去生成更多的数量（可能）

        //声明临时变量
        var oldDiffLv = currentDiffiLv;

        if (currentWave == 0)
        {
            //一些第一波的特殊处理
            //不计算“上一波”的得分
            //难度重置为1，不启用难度变化展示
            currentTime = limitTime;
            oldDiffLv = 1;
        }
        // 计算旧数据
        ChangeUserNum(vecNum);
        vecNum = 0;

        currentTotalScore += (int)((limitTime - currentTime) * 100 * (0.6f + currentDiffiLv * 0.4f));
        UserData.ChangeUTIIEW(currentWave, currentTotalScore);
        // CompareUserTopScoreInEveryWave(currentWave, currentTotalScore);//每一轮都要对比是不是历来最高的


        currentWave += 1;

        currentDiffiLv = Mathf.CeilToInt(currentWave / 10f);
        limitTime = Mathf.Max(4 - currentDiffiLv * 0.4f, 1.5f);
        currentTime = limitTime;


        //生成ui
        myUIObj.transform.Find("Canvas/CompareArea/CurrentWave/Num").GetComponent<Text>().text = currentWave.ToString();
        myUIObj.transform.Find("Canvas/CompareArea/CurrentDiffiLv/Num").GetComponent<Text>().text = currentDiffiLv.ToString();
        if (currentDiffiLv != oldDiffLv)
        {
            myUIObj.GetComponent<Animator>().Play("UI_ChangeDiffiLv");
            //myUIObj.transform.Find("Canvas/CurrentDiffiLv/Num").GetComponent<Text>().text = currentDiffiLv.ToString();
        }
        myUIObj.transform.Find("Canvas/CompareArea/CurrentTotalScore/Num").GetComponent<Text>().text = currentTotalScore.ToString();

        //生成敌人
        lowerCount = 1;
        enemyCount = 4;
        enemyInitPosList[0] = new Vector3(-1.5f, 0, 0) + gameStartPos;
        enemyInitPosList[1] = new Vector3(0, -1.5f, 0) + gameStartPos;
        enemyInitPosList[2] = new Vector3(1.5f, 0, 0) + gameStartPos;
        enemyInitPosList[3] = new Vector3(0, 1.5f, 0) + gameStartPos;
        RegenerateNumList(lowerCount);
    }

    // IEnumerator ChangeDiffLv()
    // {
    //     var x = myUIObj.transform.Find("Canvas/CurrentDiffiLv/Num").GetComponent<RectTransform>().position.y;
    //     while(x<)
    // }

    /// <summary>
    /// 生成新的数字列表
    /// </summary>
    void RegenerateNumList(int lower)
    {
        numList.Clear();
        var userNum = UserNum;
        for (int i = 0; i < lower; i++)
        {
            numList.Add(UnityEngine.Random.Range((int)Mathf.Max(userNum - 1000, userNum * 0.5f), userNum + 1));
            //Debug.Log("success Add numList!");
        }
        for (int i = 0; i < enemyCount - lower; i++)
        {
            numList.Add(UnityEngine.Random.Range(userNum + 1, (int)Mathf.Min(userNum + 1000, userNum * 1.2f)));
            //Debug.Log("success Add numList!");
        }
        //Debug.Log($"success Generate numList!{numList.Count}");
        RefreshEnemy(enemyCount, numList, enemyInitPosList);
    }

    /// <summary>
    /// 从数字清单中删除碰撞项、将碰撞数字给予波次完成后的数字增量，并且判断波次是否成功
    /// </summary>
    /// <param name="num"></param>
    public void RemoveNumList(int num)
    {
        // vecNum += num;
        numList.Remove(num);
        //Debug.Log($"RemoveNumList中，numList.count:{numList.Count}");
        CheckWaveSuccess();
    }

    /// <summary>
    /// 清除并创建敌人
    /// </summary>
    /// <param name="enemyCount"></param>
    /// <param name="numList"></param>
    /// <param name="enemyPos"></param>
    void RefreshEnemy(int enemyCount, List<int> numList, List<Vector3> enemyPos)
    {
        ClearEnemy();

        List<int> preList = new List<int>();
        foreach (var i in numList)
        {
            preList.Add(i);
        }
        //preList.ForEach(n => Debug.Log(n));
        for (int i = 0; i < enemyCount; i++)
        {
            GameObject Enemy = Instantiate(enemyPrefab, enemyPos[i], Quaternion.Euler(0, 0, 0), enemysPathObj.transform);
            enemyList.Add(Enemy);
            int x = Random.Range(0, preList.Count);

            Enemy.GetComponent<Enemy>().num = preList[x];

            preList.Remove(preList[x]);

            //数字在显露之后会慢慢消失
            //不限制数量（一开始是已知的，考察的是快速捕捉能力，短时记忆）
            var fadeRan = Random.Range(0, 1f);
            var fadeTar = Mathf.Clamp(-1f + currentDiffiLv * 0.2f, 0, 0.8f);
            if (fadeRan <= fadeTar)
            {
                Enemy.GetComponent<Enemy>().eNEMYTYPE = global::Enemy.ENEMYTYPE.FADE;
            }
        }

        //高难度的时候会出现被遮住的数字，这些数字从一开始就是被遮住的，需要判断其他的数字是不是都比你大才可以
        //被遮住的数字最多为1个（一开始就是未知的，考察的是对别的数字的观察）
        //出现概率：Clamp（（-0.3+难度*0.1），0，0.5）

        var ran = Random.Range(0, 1f);
        var tar = Mathf.Clamp(-0.2f + currentDiffiLv * 0.1f, 0, 0.9f);
        if (ran <= tar)
        {
            var targetEnemySeq = Random.Range(0, enemyList.Count);
            var targetEnemy = enemyList[targetEnemySeq];
            targetEnemy.GetComponent<Enemy>().eNEMYTYPE = Enemy.ENEMYTYPE.INVISIBLE;
            Debug.Log("success");
        }


    }



    /// <summary>
    /// 清除敌人
    /// </summary>
    void ClearEnemy()
    {
        enemyList.Clear();
        foreach (Transform child in enemysPathObj.transform)
        {
            Utilities.Common.DestroyWithAnim(child.gameObject.GetComponent<Animator>(), "Enemy_Destroy");
        }
    }

    /// <summary>
    /// 创建玩家
    /// </summary>
    void RegeneratePlayer(Vector3 pos)
    {
        playerPathObj.transform.localPosition = pos - Vector3.forward;
        if (playerObj != null) Destroy(playerObj);
        playerObj = Instantiate(playerPrefab, pos, Quaternion.Euler(0, 0, 0), playerPathObj.transform);
    }

    /// <summary>
    /// 玩家的数字是不是比其他数字更大
    /// </summary>
    /// <param name="other"></param>
    /// <returns></returns>
    public bool IsPlayerBiggerThanOther(int other)
    {
        if (UserNum >= other)
        {
            //"玩家更大";
            return true;
        }
        else
        {
            //"玩家更小";
            return false;
        }
    }

    /// <summary>
    /// 判断该波次是否胜利
    /// </summary>
    /// <returns></returns>
    public void CheckWaveSuccess()
    {
        bool a = false;//到底有没有比玩家大的
        foreach (var n in numList)
        {
            if (IsPlayerBiggerThanOther(n))
            {
                a = true;
            }
            // return false;
        }
        if (a == false)
        {
            //第一秒是满分，之后会开始降低最少是最低分
            var _minVec = 5;
            var _maxVec = 10;
            vecNum += (int)(_minVec + Mathf.Min(1, currentTime / (limitTime - 1)) * (_maxVec - _minVec));//玩家方块数字的算法
            NewWave();
        }
        else
        {
            Debug.LogError($"仍存在比玩家更大的单位，检查执行顺序，当前玩家数据：{UserNum},列表长度:{numList.Count}");
        }

        // return true;
    }


    /// <summary>
    /// 重新开始
    /// </summary>
    public void Restart()
    {
        //SceneManager.LoadScene("GameArea");

        Time.timeScale = 1;
        currDPRemain = Mathf.Min(1, Utilities.UserData.UserCurrDeathProtect);
        //FlagSet
        //StopAllCoroutines();
        gAMESTATE = GAMESTATE.GAMEONCALL;
        gAMEOVERTYPE = GAMEOVERTYPE.NULL;

        RegistUserNum();
        IUserInput.mouseDownPos = Input.mousePosition;
        IUserInput.mousedrag = IUserInput.MOUSEDRAG.NULL;
        playerObj.GetComponent<Animator>().Play("Player_Idle");

        //DataSet
        currentWave = initWave;
        vecNum = 0;
        currentTotalScore = initScore;
        //limitTime = 3;
        Time.timeScale = 1;
        //MainCamObj.transform.position = Vector3.back * 10;

        //OtherRestart
        m_onGameRestartOther();
        //timeLimitObj.GetComponent<TimeLimit>().Restart();

        //收尾
        gameOverObj.SetActive(false);

        gameStartPos = Vector3.forward;
        RegeneratePlayer(gameStartPos);
        NewWave();
    }




    /// <summary>
    /// 重新注册玩家数字
    /// </summary>
    void RegistUserNum()
    {
        UserNum = 3;//初始为3
    }

    /// <summary>
    /// 改变玩家数字
    /// </summary>
    /// <param name="changeNum"></param>
    void ChangeUserNum(int changeNum)
    {
        UserNum += changeNum;
    }

}
