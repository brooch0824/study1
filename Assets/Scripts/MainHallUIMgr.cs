using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

public class MainHallUIMgr : MonoBehaviour
{
    public List<string> names = new List<string>();
    public GameObject m_BuffDPObj;
    public GameObject m_BuffDSObj;
    Text text;

    void Start()
    {
        text = transform.Find("Title").GetComponent<Text>();
        RandomTitle();
        CheckBuff();
    }

    void RandomTitle()
    {
        var x = Random.Range(0, names.Count);
        text.text = names[x];
    }

    void CheckBuff()
    {
        //死亡保护
        var currentDPColor = m_BuffDPObj.transform.Find("Image").GetComponent<Image>().color;
        currentDPColor.a = UserData.UserCurrDeathProtect > 0 ? 1 : 0.5f;
        m_BuffDPObj.transform.Find("Image").GetComponent<Image>().color = currentDPColor;

        var currentDPTextColor = m_BuffDPObj.transform.Find("Text").GetComponent<Text>().color;
        currentDPTextColor.a = UserData.UserCurrDeathProtect > 0 ? 1 : 0.5f;
        m_BuffDPObj.transform.Find("Text").GetComponent<Text>().color = currentDPTextColor;

        m_BuffDPObj.transform.Find("Text").GetComponent<Text>().text = UserData.UserCurrDeathProtect > 0 ? "死亡保护：开" : "死亡保护：关";

    }
}
