using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GameOver : MonoBehaviour
{
    float finalWaveInit;//初始值
    float finalScoreInit;
    float finalNumberInit;
    string finalTipsInit;

    Text finalWaveText;
    Text finalScoreText;
    Text finalNumberText;
    Text finalTipsText;

    // Start is called before the first frame update

    void Awake()
    {
        finalWaveText = transform.Find("Infos/Wave/Image/Text").GetComponent<Text>();
        finalScoreText = transform.Find("Infos/Score/Image/Text").GetComponent<Text>();
        finalNumberText = transform.Find("Infos/Number/Image/Text").GetComponent<Text>();
        finalTipsText = transform.Find("Decorate/Decorate_2/Text").GetComponent<Text>();
    }

    void OnEnable()
    {
        finalWaveInit = 0;
        finalScoreInit = 0;
        finalNumberInit = 0;
        finalScoreInit = 0;
        finalTipsInit = finalTipsText.text;
    }

    void CheckFinalInfo()
    {
        StartCoroutine(CheckFinal(finalWaveText, finalWaveInit, GameManager.currentWave));
        StartCoroutine(CheckFinal(finalScoreText, finalScoreInit, GameManager.currentTotalScore));
        StartCoroutine(CheckFinal(finalNumberText, finalNumberInit, GameManager.UserNum));
    }
    void CheckFinalTips()
    {
        StartCoroutine(CheckFinal(finalTipsText));
    }

    IEnumerator CheckFinal(Text text, float now, float target)
    {
        var i = 0;
        //Debug.Log($"CheckFinal,finalWave:{finalWave},target:{GameManager.currentWave}");
        // yield return new WaitForSecondsRealtime(3f);
        while (i < 200  /*|| finalScore != TargetScore*/)
        {
            i++;
            now = Mathf.Lerp(now, target, 0.05f);
            text.text = now.ToString("f0");
            yield return null;
        }
    }
    IEnumerator CheckFinal(Text text)
    {
        // if (offset > 0)
        // {
        //     Debug.LogError("偏移值不能大于0");
        // }
        switch (GameManager.gAMEOVERTYPE)
        {
            case GameManager.GAMEOVERTYPE.TIMELIMIT:
                finalTipsInit = "似乎是<color=#FF3333> 时间不够 </color>…";
                break;
            case GameManager.GAMEOVERTYPE.WRONGHIT:
                finalTipsInit = "看来<color=#FF3333> 无法撞倒比我数字大 </color>的单位呢…";
                break;
            default:
                finalTipsInit = "欸——这句话不应该出现在这里欸…联系一下开发者吧~qq:995267940";
                break;
        }
        // yield return new WaitForSecondsRealtime(6f);
        //乱码变成真实字符，我想逐字出现的，但这个好像有点麻烦嗷我想想怎么办
        var i = 1;
        var preStr = finalTipsInit;
        while (i < 40)
        {
            text.text = string.Join("", preStr.Select(n => (char)(n + i)));
            //yield return new WaitForSecondsRealtime(0.1f);
            //Debug.Log($"字符串偏移求和:{i + offset}");
            i++;
            yield return null;
        }

        yield return new WaitForSecondsRealtime(0.5f);
        i = 1;
        preStr = text.text;
        while (i < 20)
        {
            text.text = string.Join("", preStr.Select(n => (char)(n + i)));
            i++;
            yield return new WaitForSecondsRealtime(0.02f);
            yield return null;
        }

        text.text = finalTipsInit;


        yield return new WaitForSecondsRealtime(3f);
        while (true)
        {
            var j = 1;
            while (j < 8)
            {
                text.text = string.Join("", text.text.Select(n => (char)(n + j)));
                j++;
                yield return null;
            }
            text.text = finalTipsInit;
            yield return null;
            yield return new WaitForSecondsRealtime(1.5f);
        }
        //Debug.LogError($"lerpOff+offset:{x}");
    }

}
